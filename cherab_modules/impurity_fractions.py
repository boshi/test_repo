import numpy as np

from modules.species_parser import *
from cherab.core.atomic import lookup_element, lookup_isotope
from cherab.core.atomic.elements import Isotope, Element

def get_element(element : str):

    if  (not isinstance(element, Isotope)) and (not isinstance(element, Element)):

        with open('modules/chemistry.dat', "r") as datafile:
            data = np.loadtxt(datafile, dtype=str)

        symbols  = data[:, 0]
        elements = data[:, 1]

        if (element.lower() not in symbols) and (element.lower() not in elements):
            print('None')
            return None
        elif  element in elements:
            element = symbols[elements == element][0]

        element = element.lower()

        try:
            element = lookup_element(element)
        except:
            element = lookup_isotope(element)

    else:
        print('Skipping')

    return element

def species_populate(filepath):
    gas, impurities, fractions = species_parser(filepath)

    try:
        main_gas = lookup_element(gas)
    except:
        main_gas = lookup_isotope(gas)

    impurity_fractions = {}

    for impuritie, fraction in zip(impurities, fractions):
        try:
            element = lookup_element(impuritie)
            impurity_fractions.update({element : fraction})
        except:
            element = lookup_isotope(impuritie)
            impurity_fractions.update({element : fraction})

    return main_gas, impurity_fractions