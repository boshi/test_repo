import numpy as np

from matplotlib import pyplot as plt
from matplotlib.ticker import MultipleLocator

from raysect.optical import Point3D
from raysect.core.math import Point3D


from cherab.core.math import sample3d
from cherab.core.atomic.elements import hydrogen, carbon
from cherab.tools.equilibrium import plot_equilibrium

def plot_beam(beam_full,beam_half = None, beam_third = None, limits = None, norm = False, figsize=(15,12), axis = 'z', scale = 'log'):
    fig, ax = plt.subplots(2,1, figsize=figsize, constrained_layout=True)
    
    beam_length = beam_full.length

    x_start, x_end, y_start, y_end, z_start, z_end = 0, 0, 0, 0, 0, 0
    x_num, y_num, z_num  = 1, 1, 1

    radius = 3 * beam_full.sigma

    if axis == 'x':
        x_start         = -radius if limits is None else limits[0]
        x_end           =  radius if limits is None else limits[-1]
        x_num           = 200
        ax_min          = x_start
        ax_max          = x_end

    elif axis == 'y':
        y_start         = -radius if limits is None else limits[0]
        y_end           =  radius if limits is None else limits[-1]
        y_num           = 200
        ax_min          = y_start
        ax_max          = y_end
    
    else:
        z_start         = 0  if limits is None else limits[0]
        z_end           = beam_length if limits is None else limits[-1]
        z_num           = 200
        ax_min          = z_start
        ax_max          = z_end


    # Sampling main component  
    x1, y1, z1, beam_density1D_full  = sample3d(beam_full.density, (x_start, x_end, x_num), (y_start, y_end, y_num), (z_start, z_end, z_num))
    x2, _, z2, beam_density2D_full = sample3d(beam_full.density, (-radius, radius, 100), (0, 0, 1), (0, beam_length, 500))

    # Preallocation
    beam_density2D_half  = np.zeros(beam_density2D_full.shape)
    beam_density2D_third = np.zeros(beam_density2D_full.shape)

    # Plotting main component density
    if norm == True:
        n_coef_1 = np.max(beam_density1D_full)
    else:
        n_coef_1  = 1

    axis_grid = z1
    if axis == 'x':
        axis_grid = x1
    if axis == 'y':
        axis_grid = y1
    
    if scale == 'log':
        ax[1].semilogy(axis_grid,np.squeeze(np.squeeze(beam_density1D_full / n_coef_1)),label=f'{(beam_full.energy/1e3):.1f} кэВ')
    elif scale == 'linear':
        ax[1].plot(axis_grid,np.squeeze(np.squeeze(beam_density1D_full / n_coef_1)),label='E0')
    
    # Sampling E0/2
    if beam_half != None:
        x1, y1, z1, beam_density1D_half  = sample3d(beam_half.density, (x_start, x_end, x_num), (y_start, y_end, y_num), (z_start, z_end, z_num))
        x2, _, z2, beam_density2D_half = sample3d(beam_half.density, (-radius, radius, 100), (0, 0, 1), (0, beam_length, 500))
        if norm == True:
            n_coef_2 = np.max(beam_density1D_half)
        else:
            n_coef_2  = 1
    
        if scale == 'log':
            ax[1].semilogy(axis_grid,np.squeeze(np.squeeze(beam_density1D_half / n_coef_2)),label=f'{(beam_half.energy/1e3):.1f} кэВ')
        elif scale == 'linear':
            ax[1].plot(axis_grid,np.squeeze(np.squeeze(beam_density1D_half / n_coef_2)),label='E0/2')


    # Sampling E0/3	
    if beam_third != None:
        x1, y1, z1, beam_density1D_third = sample3d(beam_third.density,(x_start, x_end, x_num), (y_start, y_end, y_num), (z_start, z_end, z_num))
        x2, _, z2, beam_density2D_third = sample3d(beam_third.density, (-radius, radius, 100), (0, 0, 1), (0, beam_length, 500))
        if norm == True:
            n_coef_3 = np.max(beam_density1D_third)
        else:
            n_coef_3  = 1

        if scale == 'log':
            ax[1].semilogy(axis_grid,np.squeeze(np.squeeze(beam_density1D_third / n_coef_3)),label=f'{(beam_third.energy/1e3):.1f} кэВ')
        elif scale == 'linear':	
            ax[1].plot(axis_grid,np.squeeze(np.squeeze(beam_density1D_third / n_coef_3)),label='E0/3')

    ax[1].set_xlabel('Расстояние от фланца инжектора, м', fontsize=16)
    if norm == False:
        ax[1].set_ylabel(r'Плотность пучка, м$^{-3}$', fontsize=16)
    else:
        ax[1].set_ylabel('Доля начальной плотности пучка', fontsize=16)
        if scale == 'linear':
            ax[1].yaxis.set_major_locator(MultipleLocator(beam_length / 20))

    ax[1].set_xlim(ax_min,ax_max)
    ax[1].tick_params(axis='x', labelsize=14)
    ax[1].tick_params(axis='y', labelsize=14)

    if axis == 'z':
        ax[1].xaxis.set_major_locator(MultipleLocator(beam_length / 20))
    else:
        ax[1].xaxis.set_major_locator(MultipleLocator(radius / 20))

    beam_density2D = beam_density2D_full + beam_density2D_half + beam_density2D_third
    cs = ax[0].contourf(z2,x2,(np.squeeze(beam_density2D)),35,cmap="inferno")
    cbar = fig.colorbar(cs,ax=ax[0])
    cbar.set_label(r'Полная концентрация пучка, м$^{-3}$')

    ax[0].set_xlabel('z пучка, м', fontsize=16)
    ax[0].set_ylabel('x пучка, м', fontsize=16)
    ax[0].set_xlim(0,beam_length)
    ax[0].xaxis.set_major_locator(MultipleLocator(beam_length / 20))
    ax[0].grid(visible=True, which='both', axis='both', color = 'white')

    ax[0].tick_params(axis='x', labelsize=14)
    ax[0].tick_params(axis='y', labelsize=14)

    ax[1].grid(visible=True, which='both', axis='both')

    plt.legend()



def plot_beam_toroidal(list_of_beams, equilibrium, norm = False, z_position = None, fig_size=(15,15), scale = 'log'):

    MA_r , MA_z  = equilibrium.magnetic_axis
    R_min, R_max = equilibrium.r_range
    Z_min, Z_max = equilibrium.z_range

    if not z_position:
        z_position = MA_z

    x_start, x_end, x_num = -R_max, R_max, 2000
    y_start, y_end, y_num = -R_max, R_max, 2000

    beam_density = np.zeros((x_num, y_num))
    xpts = np.linspace(x_start, x_end, x_num)
    ypts = np.linspace(y_start, y_end, y_num)

    # Adding port central lines
    angles = np.arange(0, 361, 22.5)
    rad_1 = np.ones(len(angles)) * 1.5
    rad_2 = np.ones(len(angles)) * 2.93

    x1 = rad_1 * np.cos(np.deg2rad(angles))
    y1 = rad_1 * np.sin(np.deg2rad(angles))
    x2 = rad_2 * np.cos(np.deg2rad(angles))
    y2 = rad_2 * np.sin(np.deg2rad(angles))
    

    for beam in list_of_beams:
        for i, xpt in enumerate(xpts):
            for j, ypt in enumerate(ypts):
                pt = Point3D(xpt, ypt, z_position).transform(beam.to_local())
                beam_density[i, j] = beam_density[i, j] + beam.density(pt.x, pt.y, pt.z)

    figure, axes = plt.subplots(figsize=fig_size)
    plt.rcParams.update({'font.size': 16})
    owter_wall = plt.Circle(( 0. , 0. ), 2.925, color='w', fill = False, linewidth=2) 
    inner_wall = plt.Circle(( 0. , 0. ), 1.5,   color='w', fill = False, linewidth=2) 
    magnetic_axis = plt.Circle(( 0. , 0. ), 2.15, color='r', fill = False, linewidth=2) 
    outer_plasma_border = plt.Circle(( 0. , 0. ), 2.686, color='c', fill = False, linewidth=3) 
    inner_plasma_border = plt.Circle(( 0. , 0. ), 1.588, color='c', fill = False, linewidth=3)
    if scale == 'log':
        plotted_beam_density = np.log10(beam_density)
        vmin_setting = 12
    else:
        plotted_beam_density = beam_density
        vmin_setting = 0

    plt.imshow(np.transpose(plotted_beam_density), extent=[x_start, x_end, y_start, y_end], origin='lower', cmap='inferno', vmin = vmin_setting)
    plt.yticks(fontsize=16)
    plt.xticks(fontsize=16)
    axes.add_artist(owter_wall)
    axes.add_artist(inner_wall)
    axes.add_artist(magnetic_axis)
    axes.add_artist(outer_plasma_border)
    axes.add_artist(inner_plasma_border)
    axes.set_facecolor("black")
    for i in range(len(x2)):
        plt.plot([x1[i],x2[i]], [y1[i],y2[i]], linestyle='--', color='w')
    plt.colorbar()
    plt.axis('equal')
    return figure
    



def plot_plasma_on_R(plasma,equilibrium, main_ion = hydrogen, main_ion_charge = 1,
                       impurity_ion = carbon, impurity_charge = 6,
                       impurity_only = False):

    ion=plasma.composition.get(main_ion,main_ion_charge)
    imp=plasma.composition.get(impurity_ion,impurity_charge)

    _, MA_z = equilibrium.magnetic_axis
    R_min, R_max =  equilibrium.r_range

    if not impurity_only:
        R, _, _, ne = sample3d(plasma.electron_distribution.density,(R_min,R_max,100),(0,0,1),(MA_z,MA_z,1))
        R, _, _, ni = sample3d(plasma.ion_density,(R_min,R_max,100),(0,0,1),(MA_z,MA_z,1))
        ne = np.squeeze(np.squeeze(ne))
        ni = np.squeeze(np.squeeze(ni))

        R, _, _, Te = sample3d(plasma.electron_distribution.effective_temperature,(R_min,R_max,100),(0,0,1),(MA_z,MA_z,1))
        R, _, _, Ti_ion = sample3d(ion.distribution.effective_temperature,(R_min,R_max,100),(0,0,1),(MA_z,MA_z,1))
        R, _, _, Ti_imp = sample3d(imp.distribution.effective_temperature,(R_min,R_max,100),(0,0,1),(MA_z,MA_z,1))
        Te = np.squeeze(np.squeeze(Te))
        Ti_ion = np.squeeze(np.squeeze(Ti_ion))
        Ti_imp = np.squeeze(np.squeeze(Ti_imp))
       
        fig, ax = plt.subplots(1,2, figsize=(15,10))
        ax[0].plot(R,ne,label = 'ne')
        ax[0].plot(R,ni,label='sum(ni)')
        ax[0].set_xlabel('R, m')
        ax[0].set_ylabel('density, m-3')
        ax[0].legend()
        ax[0].grid(True)

        ax[1].plot(R,Te,label = 'Te')
        ax[1].plot(R,Ti_ion,label='Ti_'+main_ion.name+str(main_ion_charge)+'+')
        ax[1].plot(R,Ti_imp,label='Ti_'+impurity_ion.name+str(impurity_charge)+'+')
        ax[1].set_xlabel('R, m')
        ax[1].set_ylabel('temperature, eV')
        ax[1].legend()
        ax[1].grid(True)
    else:
        R, _, _, nz = sample3d(imp.distribution.density,(R_min,R_max,100),(0,0,1),(MA_z,MA_z,1))
        R, _, _, Ti_imp = sample3d(imp.distribution.effective_temperature,(R_min,R_max,100),(0,0,1),(MA_z,MA_z,1))
        nz = np.squeeze(np.squeeze(nz))
        Ti_imp = np.squeeze(np.squeeze(Ti_imp))
       
        fig, ax = plt.subplots(1,2, figsize=(15,10))
        ax[0].plot(R,nz,label=impurity_ion.name+str(impurity_charge)+'+'+' density')
        ax[0].set_xlabel('R, m')
        ax[0].set_ylabel('density, m-3')
        ax[0].legend()
        ax[0].grid(True)

        ax[1].plot(R,Ti_imp,label='Ti_'+impurity_ion.name+str(impurity_charge)+'+')
        ax[1].set_xlabel('R, m')
        ax[1].set_ylabel('temperature, eV')
        ax[1].legend()
        ax[1].grid(True)

    fig.show()
     

def plot_created_plasma(plasma, equilibrium, impurity_verbose = False, eqdsk_verbose = False):
    RA_z, MA_z = equilibrium.magnetic_axis
    R_min, R_max = equilibrium.r_range
    Z_min, Z_max = equilibrium.z_range
    
    if impurity_verbose:
        fig, ax = plt.subplots(1,3, figsize=[18,7.5], constrained_layout=True)
        fig.suptitle('Plasma parameters in logarithmic scale')

        mainnes_dict = {}
        for species in plasma.composition:
            mainnes_dict[species] = species.distribution.density(RA_z, 0, MA_z)

        sorted_mainnes_dict = {k: v for k, v in sorted(mainnes_dict.items(), key=lambda item: item[1])}
        sorted_mainnes_list = list(sorted_mainnes_dict)

        flag = True
        i = -2
        while flag:
            if sorted_mainnes_list[i].element != sorted_mainnes_list[-1].element:
                main_impurity = sorted_mainnes_list[i]
                imp=plasma.composition.get(main_impurity.element,main_impurity.charge)
                flag = False
            else:
                i -=1

            if i == -1 * len(sorted_mainnes_list):
                flag == False

        x2, _, z2, main_impurity2D_full = sample3d(imp.distribution.density, (R_min, R_max, 400), (0, 0, 1), (Z_min, Z_max, 800))
        log_imp_density = np.log10(main_impurity2D_full).transpose()
        cs2 = ax[2].contourf(x2,z2,(np.squeeze(log_imp_density)),80,cmap="inferno")
        ax[2].set_aspect('equal', 'box')
        cbar2 = fig.colorbar(cs2,ax=ax[2])
        cbar2.set_label(rf'{main_impurity.element.name} density, m$^{-3}$')
        ax[2].set_xlabel('R, m')
        ax[2].set_ylabel('Z, m')

    else:
        fig, ax = plt.subplots(1,2, figsize=[12,7.5], constrained_layout=True)
    
    x2, _, z2, electron_density2D_full = sample3d(plasma.electron_distribution.density, (R_min, R_max, 400), (0, 0, 1), (Z_min, Z_max, 800))
    log_electron_density = np.log10(electron_density2D_full).transpose()
    cs0 = ax[0].contourf(x2,z2,(np.squeeze(log_electron_density)),80,cmap="inferno")
    ax[0].set_aspect('equal', 'box')
    cbar0 = fig.colorbar(cs0,ax=ax[0])
    cbar0.set_label(r'Electron density, m$^{-3}$')
    ax[0].set_xlabel('R, m')
    ax[0].set_ylabel('Z, m')

    x2, _, z2, electron_temperature2D_full = sample3d(plasma.electron_distribution.effective_temperature, (R_min, R_max, 400), (0, 0, 1), (Z_min, Z_max, 800))
    log_electron_temperature = np.log10(electron_temperature2D_full).transpose()
    cs1 = ax[1].contourf(x2,z2,(np.squeeze(log_electron_temperature)),80,cmap="inferno")
    ax[1].set_aspect('equal', 'box')
    cbar1 = fig.colorbar(cs1,ax=ax[1])
    cbar1.set_label(r'Electron temperature, eV')
    ax[1].set_xlabel('R, m')
    ax[1].set_ylabel('Z, m')

    plt.show()

    if eqdsk_verbose:
        plot_equilibrium(equilibrium, detail=False, resolution=0.005)