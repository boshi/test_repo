
import numpy as np
from scipy.constants import atomic_mass, electron_mass

from raysect.core.math.function.float import Blend2D, Interpolator1DArray, Constant2D
from raysect.optical import translate, Vector3D
from raysect.primitive import Cylinder, Subtract

from cherab.core import Plasma, Species, Maxwellian
from cherab.core.math import AxisymmetricMapper, VectorAxisymmetricMapper
from cherab.core.math.function import ConstantVector3D
from cherab.core.atomic.elements import lookup_isotope, lookup_element
from cherab.tools.plasmas.ionisation_balance import from_elementdensity, match_plasma_neutrality
from cherab.openadas import OpenADAS


def create_blended_plasma(core_sim, edge_sim, b_field, blending_mask, parent=None, name=None, transform=None):
    """
    Creates a blended plasma object from the edge plasma simulation and core profiles.

    :param dict core_sim: Core simulation dictionary, containing the keys:
                          'ne' for the electron density as Function2D,
                          'te' for the electron temperature as Function2D,
                          'ti' for the ion temperature as Function2D,
                          'ns' for the species density as dictionary:
                          {'(element_name, charge)' : density_function_2d}
    :param SOLPSSimulation/EDGE2DSimulation edge_sim: Cherab edge plasma simulation object.
    :param VectorFunction2D b_field: Magnetic field in cylindrical coordinates.
    :param Function2D blending_mask: Modulating function for blending core and edge profiels.
    :param Node parent: Parent node in the scenegraph.
    :param str name: The name of the plasma object.
    :param AffineMatrix3D transform: Coordinate transformation matrix.
    """

    # Blending edge and core profiles using modulating function.
    ne = Blend2D(edge_sim.electron_density_f2d, core_sim['ne'], blending_mask)
    te = Blend2D(edge_sim.electron_temperature_f2d, core_sim['te'], blending_mask)
    ti = Blend2D(edge_sim.ion_temperature_f2d, core_sim['ti'], blending_mask)

    species_list = set(edge_sim.species_list + tuple(core_sim['ns'].keys()))
    species_density = {}
    for sp in species_list:
        if sp in core_sim['ns'] and sp in edge_sim.species_list:
            species_density[sp] = Blend2D(edge_sim.species_density_f2d[sp], core_sim['ns'][sp], blending_mask)
        elif sp in edge_sim.species_list:  # blending with zero to match plasma neutrality
            species_density[sp] = Blend2D(edge_sim.species_density_f2d[sp], Constant2D(0), blending_mask)
        else:
            species_density[sp] = Blend2D(Constant2D(0), core_sim['ns'][sp], blending_mask)

    neutral_list = [sp for sp in species_list if sp[1] == 0]
    neutral_temperature = {}
    for sp in neutral_list:
        if sp in edge_sim.neutral_list and edge_sim.neutral_temperature is not None:
            neutral_temperature[sp] = Blend2D(edge_sim.neutral_temperature_f2d[sp], core_sim['ti'], blending_mask)
        else:
            neutral_temperature[sp] = ti

    # The plasma geometry is determined by the extent of the edge simulation mesh
    mesh = edge_sim.mesh
    name = name or "Core+Edge Plasma"
    plasma = Plasma(parent=parent, transform=transform, name=name)
    radius_max = mesh.mesh_extent['maxr']
    radius_min = mesh.mesh_extent['minr']
    height = mesh.mesh_extent['maxz'] - mesh.mesh_extent['minz']
    plasma.geometry = Subtract(Cylinder(radius_max, height), Cylinder(radius_min, height))
    plasma.geometry_transform = translate(0, 0, mesh.mesh_extent['minz'])

    # Magnetic field
    plasma.b_field = VectorAxisymmetricMapper(b_field)

    # Electron distribution
    if edge_sim.electron_velocities_cartesian is None:
        electron_velocity = ConstantVector3D(Vector3D(0, 0, 0))
    else:
        electron_velocity = edge_sim.electron_velocities_cartesian
    plasma.electron_distribution = Maxwellian(AxisymmetricMapper(ne), AxisymmetricMapper(te), electron_velocity, electron_mass)

    # Species distributions
    for sp in species_list:

        try:
            species_type = lookup_element(sp[0])
        except ValueError:
            species_type = lookup_isotope(sp[0])

        charge = sp[1]

        if edge_sim.velocities_cartesian is not None and sp in edge_sim.species_list:
            velocity = edge_sim.velocities_cartesian[sp]
        else:
            velocity = ConstantVector3D(Vector3D(0, 0, 0))

        # ions
        if charge:
            distribution = Maxwellian(AxisymmetricMapper(species_density[sp]), AxisymmetricMapper(ti), velocity,
                                      species_type.atomic_weight * atomic_mass)
        # neutral atoms
        else:
            distribution = Maxwellian(AxisymmetricMapper(species_density[sp]), AxisymmetricMapper(neutral_temperature[sp]), velocity,
                                      species_type.atomic_weight * atomic_mass)

        plasma.composition.add(Species(species_type, charge, distribution))

    return plasma

def create_core_plasma(core_sim, equilibrium, parent=None, name=None, transform=None):
    """
    Creates a blended plasma object from the edge plasma simulation and core profiles.

    :param dict core_sim: Core simulation dictionary, containing the keys:
                          'ne' for the electron density as Function2D,
                          'te' for the electron temperature as Function2D,
                          'ti' for the ion temperature as Function2D,
                          'ns' for the species density as dictionary:
                          {'(element_name, charge)' : density_function_2d}
    :param SOLPSSimulation/EDGE2DSimulation edge_sim: Cherab edge plasma simulation object.
    :param VectorFunction2D b_field: Magnetic field in cylindrical coordinates.
    :param Function2D blending_mask: Modulating function for blending core and edge profiels.
    :param Node parent: Parent node in the scenegraph.
    :param str name: The name of the plasma object.
    :param AffineMatrix3D transform: Coordinate transformation matrix.
    """

    b_field = equilibrium.b_field

    # Blending edge and core profiles using modulating function.
    ne = core_sim['ne']
    te = core_sim['te']
    ti = core_sim['ti']

    species_list = tuple(core_sim['ns'].keys())
    species_density = {}
    for sp in species_list:
        species_density[sp] = core_sim['ns'][sp]


    neutral_list = [sp for sp in species_list if sp[1] == 0]
    neutral_temperature = {}
    for sp in neutral_list:
        neutral_temperature[sp] = ti

    # The plasma geometry is determined by the extent of the edge simulation mesh
    name = name or 'Core Plasma'
    print(type(name))
    plasma = Plasma(parent=parent, transform=transform, name=name)



    radius_min, radius_max = equilibrium.r_range
    Z_min, Z_max = equilibrium.z_range
    height = Z_max - Z_min
    plasma.geometry = Subtract(Cylinder(radius_max, height), Cylinder(radius_min, height))
    plasma.geometry_transform = translate(0, 0, Z_min)

    # Magnetic field
    plasma.b_field = VectorAxisymmetricMapper(b_field)

    # Electron distribution
    
    velocity = ConstantVector3D(Vector3D(0, 0, 0))
    electron_velocity = ConstantVector3D(Vector3D(0, 0, 0))
    plasma.electron_distribution = Maxwellian(AxisymmetricMapper(ne), AxisymmetricMapper(te), electron_velocity, electron_mass)

    # Species distributions
    for sp in species_list:

        try:
            species_type = lookup_element(sp[0])
        except ValueError:
            species_type = lookup_isotope(sp[0])

        charge = sp[1]

        # ions
        if charge:
            distribution = Maxwellian(AxisymmetricMapper(species_density[sp]), AxisymmetricMapper(ti), velocity,
                                      species_type.atomic_weight * atomic_mass)
        # neutral atoms
        else:
            distribution = Maxwellian(AxisymmetricMapper(species_density[sp]), AxisymmetricMapper(neutral_temperature[sp]), velocity,
                                      species_type.atomic_weight * atomic_mass)

        plasma.composition.add(Species(species_type, charge, distribution))

    return plasma


def load_core_simulation(core_datafile, equilibrium, impurity_fraction, bulk_element, datapath=None):
    core_data = np.loadtxt(core_datafile)

    # Get psi_norm for low-field side
    indx, = np.where(core_data[:, 0] >= 0)
    core_data = core_data[indx, :]
    rmagn, zmagn = equilibrium.magnetic_axis
    r = core_data[:, 0] + rmagn
    psin = np.array([equilibrium.psi_normalised(r1, zmagn) for r1 in r])

    ne = 1.e19 * core_data[:, 1]
    te = 1.e3 * core_data[:, 2]
    ti = 1.e3 * core_data[:, 4]

    # Solve ionization balance
    if not datapath:
        openadas = OpenADAS(permit_extrapolation=True)
    else:
        openadas = OpenADAS(permit_extrapolation=True, data_path=datapath)
        
    density_profiles = {}
    for element, fraction in impurity_fraction.items():
        density_profiles[element] = from_elementdensity(openadas, element, ne * fraction, ne, te)

    density_profiles[bulk_element] = match_plasma_neutrality(openadas, bulk_element, [v for v in density_profiles.values()], ne, te)

    # Create 2D interpolators
    sim = {}
    sim['ne'] = equilibrium.map2d(Interpolator1DArray(psin, ne, 'linear', 'nearest', 1.e-3))
    sim['te'] = equilibrium.map2d(Interpolator1DArray(psin, te, 'linear', 'nearest', 1.e-3))
    sim['ti'] = equilibrium.map2d(Interpolator1DArray(psin, ti, 'linear', 'nearest', 1.e-3))
    sim['ns'] = {}
    for element, densities in density_profiles.items():
        for chrg, density in densities.items():
            sim['ns'][(element.name, chrg)] = equilibrium.map2d(Interpolator1DArray(psin, density, 'cubic', 'nearest', 1.e-3))

    return sim


if __name__ == 'main':
    pass
