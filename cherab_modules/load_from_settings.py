from raysect.core.math.function.float import Interpolator1DArray
from raysect.primitive.mesh import Mesh
from raysect.optical import World
from raysect.optical.library.metal.roughmetal import _DataLoader as RoughLoader
from raysect.optical.library.metal.metal import _DataLoader as BareLoader
from raysect.optical.material import AbsorbingSurface, Blend

from cherab.core.model import ExcitationLine, RecombinationLine, BeamCXLine, BeamEmissionLine, MultipletLineShape, ZeemanTriplet, ZeemanMultiplet, ZeemanLineShapeModel, ParametrisedZeemanTriplet, Bremsstrahlung, StarkBroadenedLine, GaussianLine
from cherab.core.atomic import Line

from cherab.t15md.machine.material import _conducting_t15md_graphite, _lambert_t15md_graphite
from cherab.t15md.tools.parsers import load_solps_from_carre_dat

from cherab.openadas import OpenADAS
from cherab.tools.equilibrium import import_eqdsk

from cherab_modules.common import create_blended_plasma, load_core_simulation, create_core_plasma
from cherab_modules.impurity_fractions import species_populate, get_element

from modules.classes import line_of_sight, pinhole_camera, fibre_optic
from modules.gui_modules import show_warning




# ! ----------------------------------------------------------------------------
def create_plasma(files, parent = None):
    equilibrium = import_eqdsk(files.eqdsk.path)
    main_gas, impurity_fraction = species_populate(files.plasma_species.path)

    core_sim = load_core_simulation(files.plasma_core.path, equilibrium, impurity_fraction, main_gas)

    if files.plasma_edge.exist and files.mesh.exist:
        edge_sim = load_solps_from_carre_dat(files.mesh.path, files.plasma_edge.path)
        blending_mask = equilibrium.map2d(Interpolator1DArray([0, 0.935, 0.97, 1.0], [1, 1, 0, 0], 'linear', 'none', 0))

        plasma = create_blended_plasma(core_sim, edge_sim, equilibrium.b_field, blending_mask, parent)
    else:
        plasma = create_core_plasma(core_sim, equilibrium, parent)

    plasma.atomic_data = OpenADAS(permit_extrapolation=True, missing_rates_return_null=True)

    if not parent is None:
        plasma.parent = parent

    return equilibrium, plasma


def load_cad_models(model_files, transform):
    cad_models = []

    try:
        counter = 1
        for model_file in model_files:
            model_obj = Mesh.from_file(model_file.path, transform=transform)

            cad_models.append(model_obj)
            if model_file.material == 'None':
                model_obj.material = AbsorbingSurface()
            else:
                if model_file.roughness == 0:
                    roughness = 0.001
                else:
                    roughness = model_file.roughness

                if model_file.material == 'graphite':
                    model_obj.material = Blend(_conducting_t15md_graphite, _lambert_t15md_graphite, roughness, surface_only=True)
                elif model_file.material == 'steel':
                    model_obj.material = Blend(RoughLoader('iron', 0.21), _lambert_t15md_graphite, roughness, surface_only=True)
                else:
                    if model_file.roughness == 0:
                        model_obj.material = BareLoader(model_file.material)
                    else:
                        model_obj.material = RoughLoader(model_file.material, model_file.roughness)

            counter +=1
            return cad_models
    except:
            return counter


def create_plasma_lines(plasma_lines, verbose : bool = False):
    counter = 0
    list_of_lines = []
    for line_setting in plasma_lines:
        counter += 1
        try:
            line_obj = Line(get_element(line_setting.element), line_setting.charge, (line_setting.transition[-1], line_setting.transition[0]))

            if line_setting.shape == 'Gaussian':
                shape = GaussianLine
            elif line_setting.shape == 'Multiplet':
                shape = MultipletLineShape
            elif line_setting.shape == 'Stark Broadened':
                shape = StarkBroadenedLine
            elif line_setting.shape == 'Zeeman line shape':
                shape = ZeemanLineShapeModel
            elif line_setting.shape == 'Zeeman Triplet':
                shape = ZeemanTriplet
            elif line_setting.shape == 'Zeeman Multiplet':
                shape = ZeemanMultiplet
            elif line_setting.shape == 'Parametrised Triplet':
                shape = ParametrisedZeemanTriplet

            if line_setting.type == 'Exitation':
                list_of_lines.append(ExcitationLine(line_obj, lineshape=shape))
            else:
                list_of_lines.append(RecombinationLine(line_obj, lineshape=shape))

            if verbose:
                print(list_of_lines[-1])

        except:
            if verbose:
                show_warning(f'There was a problem with Plasma line {counter}')
            return counter

    return list_of_lines

def create_beam_lines(beam_lines, main_gas, verbose : bool = False):
    counter = 0
    list_of_lines = []

    for line_setting in beam_lines:
        counter += 1
        try:
            if line_setting.type == 'Exitation':
                line_obj = Line(get_element(line_setting.element), 0, (line_setting.transition[-1], line_setting.transition[0]))
                list_of_lines.append(BeamEmissionLine(line_obj, lineshape_kwargs = line_setting.shape))
            else:
                line_obj = Line(get_element(main_gas), 0, (line_setting.transition[-1], line_setting.transition[0]))
                list_of_lines.append(BeamCXLine(line_obj, lineshape_kwargs = line_setting.shape))

            if verbose:
                print(list_of_lines[-1])
        except:
            if verbose:
                show_warning(f'There was a problem with line {counter}')
            return counter

    return list_of_lines




def create_los():
    pass

def create_pinhole_camera():
    pass

def create_optical_fiber():
    pass