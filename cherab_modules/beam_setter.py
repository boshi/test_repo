from cherab.openadas import OpenADAS
from cherab.core import Beam
from cherab.core.model import SingleRayAttenuator
from cherab.core.atomic import lookup_element, lookup_isotope

from scipy.interpolate import interp1d

from raysect.core.math import Point3D
from raysect.optical import translate, rotate_basis, Vector3D

import numpy as np


def pol2cart(rho, phi):
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return(x, y)


def neutralization_efficiensy(energy):
        # Efficiency of neutralisation in percent
        filename            = r'data/neutralization.dat'
        neutralization_data = np.loadtxt(filename)
        beam_energy         = neutralization_data[:,0]
        efficiensy          = neutralization_data[:,1] / 100
        efficiensy_int      = interp1d(beam_energy, efficiensy, kind='cubic', bounds_error=False, fill_value=0.25)

        if energy > 1.e4:
            print('Assuming the energy is in eV')
            return efficiensy_int(energy/1e3)
        else:
            return efficiensy_int(energy)


def beam_setter(beam_settings, plasma = None, parent = None):
    start  = beam_settings.start
    focus  = beam_settings.focus

    beam_start  = Point3D(start[0], start[1], start[2])
    beam_focus  = Point3D(focus[0], focus[1], focus[2])
    dnb_forward = beam_start.vector_to(beam_focus).normalise()
    transform   = translate(beam_start.x, beam_start.y, beam_start.z) * rotate_basis(dnb_forward, Vector3D(0, 0, 1))

    if parent is None and not plasma is None:
        parent = plasma.parent

    beam = Beam(parent=parent, transform=transform)

    if not plasma is None:
        beam.plasma     = plasma
        beam.attenuator = SingleRayAttenuator(clamp_to_zero=False)
        beam.models     = []

    if isinstance(beam_settings.power, list):
        beam.power = beam_settings.power[0]
    else:
        beam.power = beam_settings.power

    try:
        element = lookup_element(beam_settings.element)
    except:
        element = lookup_isotope(beam_settings.element)

    beam.energy                 = beam_settings.energy
    beam.temperature            = beam_settings.temperature
    beam.element                = element
    beam.sigma                  = beam_settings.sigma
    beam.divergence_x           = beam_settings.divergence
    beam.divergence_y           = beam_settings.divergence
    beam.length                 = beam_settings.length
    beam.focal_length           = beam_settings.focal_length

    beam.atomic_data            = OpenADAS(permit_extrapolation=True, missing_rates_return_null=True)
    beam.integrator.step        = 0.0025
    beam.integrator.min_samples = 5

    return beam



def beam_list_setter(beam_settings, plasma = None, parent = None):
    if beam_settings.power[1] == 0 and beam_settings.power[2] == 0:
        return beam_setter(beam_settings, plasma, parent)
    else:
        number_of_beams = 3

        e0    = beam_settings.energy
        power = beam_settings.power

        list_of_beams = []
        for i in range(number_of_beams):
            beam_settings.power  = power[i]
            beam_settings.energy = e0 / (i+1)

            new_beam = beam_setter(beam_settings, plasma, parent)
            list_of_beams.append(new_beam)

        return list_of_beams