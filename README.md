## Cherab GUI app
This is a GUI app to set up and conduct simulations with [Cherab](https://github.com/cherab/core) framework.

The application is build in *Python* with *Qt* and supports the following:

* creating plasma object
* creating beam object
* importing meshes and applying materials to them

## Requirements

This app needs a Python environment (virtual or otherwise) with the following packages installed:

* raysect
* cherab
* cherab-solps for importing edge plasma
* cherab-t15md for graphite and steel materials
* numpy
* scipy
* matplotlib
* PySide6