import os

class stored_file:
    def __init__(self, path = None):
        self.path = path
        self.exist = None
        self.check()

    def __str__(self):
        return f'{self.path}'

    def __repr__(self):
        return f'{self.path}'

    def set(self, path):
        self.path = path
        self.check()

    def check(self, verbose = False):
        if self.path is None:
            return None
        else:
            self.exist = os.path.isfile(self.path)
            if verbose:
                if self.exist:
                    return 'exists'
                else:
                    return 'does not exist'
            else:
                return self.exist

class stored_model:
    def __init__(self, path, material, roughness):
        self.path = path
        self.material = material
        self.roughness = roughness
        self.exist = os.path.isfile(path)

    def verbose(self):
        if self.exist:
            exist_line = 'exists'
        else:
            exist_line = 'does not exist'
        print(f'CAD model: path {self.path}, {exist_line}; material {self.material}; roughness = {self.roughness}')

class setup_status:
    def __init__(self, plasma = False, vessel = False, dnb = False, nbi = False, emission = False):
        self._plasma = plasma
        self._vessel = vessel
        self._dnb = dnb
        self._nbi = nbi
        self._emission = emission

    def verbose(self):
        return [self._plasma, self._vessel, self._dnb, self._nbi, self._emission]

    def update_plasma(self, plasma):
        self._plasma = plasma
    def update_vessel(self, vessel):
        self._vessel = vessel
    def update_dnb(self, dnb):
        self._dnb = dnb
    def update_nbi(self, nbi):
        self._nbi = nbi
    def update_emission(self, emission):
        self._emission = emission

    def ready_for_emission(self):
        status_list = [self._plasma, self._dnb, self._nbi]
        if any(status is False for status in status_list):
            return False
        else:
            return True

    def ready_for_simulation(self):
        status_list = [self._plasma, self._vessel, self._dnb, self._nbi, self._emission]
        if any(status is False for status in status_list):
            return False
        else:
            return True

    def __str__(self):
        return f'Plasma is ok: {self._plasma}, vessel is ok: {self._vessel}, dnb is ok: {self._dnb}, nbi is ok: {self._nbi}, emission is ok: {self._emission}'



class transform_setting:
    def __init__(self, transform_list = None, rotate_list = None):
        if transform_list is None:
            self.transform_list = [0, 0, 0]
        else:
            self.transform_list = transform_list

        if rotate_list is None:
            self.rotate_list = [0, 0, 0]
        else:
            self.rotate_list = rotate_list


class plasma_files:
    def __init__(self):
        self.eqdsk = stored_file()
        self.plasma_species = stored_file()
        self.plasma_core = stored_file()
        self.plasma_edge = stored_file()
        self.mesh = stored_file()

    def clear(self):
        self.__init__()


    def check_all(self):
        flags = [self.eqdsk.check(), self.plasma_species.check(), self.plasma_core.check(), self.plasma_edge.check(), self.mesh.check()]
        for flag in flags:
            if flag is None or flag:
                return False
        return True

    def verbose(self):
        if not self.eqdsk.path is None:
            eqdsl_line = f'EQDSK file, {self.eqdsk.check(verbose=True)}; '
        else:
            eqdsl_line = ''

        if not self.plasma_species.path is None:
            species_line = f'Species file, {self.plasma_species.check(verbose=True)}; '
        else:
            species_line = ''

        if not self.plasma_core.path is None:
            core_line = f'Core file, {self.plasma_core.check(verbose=True)}; '
        else:
            core_line = ''

        if not self.plasma_edge.path is None:
            edge_line = f'Edge file, {self.plasma_edge.check(verbose=True)}; '
        else:
            edge_line = ''

        if not self.mesh.path is None:
            mesh_line = f'Mesh file, {self.mesh.check(verbose=True)}.'
        else:
            mesh_line = ''

        print(f'Selected plasma files: {eqdsl_line}{species_line}{core_line}{edge_line}{mesh_line}')

class beam_settings:
    def __init__(self, element = None, energy = None, radius = None, power = None, divergence = None, temperature = None, start = None, focus = None, length = None):
        self.element      = element
        self.energy       = energy
        self.radius       = radius
        self.power        = power
        self.divergence   = divergence
        self.temperature  = temperature
        self.start        = start
        self.focus        = focus
        self.length       = length
        self.focal_length = self.get_focal_length()

        if not radius is None:
            self.sigma = self.radius / (2**0.5)
        else:
            self.sigma = None

    def clear(self):
        self.__init__()

    def get_focal_length(self):
        if not self.start is None and not self.focus is None:
            r0 = (sum([point**2 for point in self.start]))**(0.5)
            r1 = (sum([point**2 for point in self.focus]))**(0.5)
            return abs(r1 - r0)
        else:
            return None

    def verbose(self):
        print(f'Beam setting: element {self.element}, energy {self.energy} keV, radius {self.radius} m, power in atoms {self.power} kW, divergenece {self.divergence} deg, temperature {self.temperature} eV, start {self.start} m, focus {self.focus} m, length {self.length} m')


class line_settings:
    def __init__(self, element : str, charge : int, h_lvl : int, l_lvl : int, line_type : str, line_shape : str):
        self.element = element
        self.charge  = charge
        self._l_lvl = l_lvl
        self._h_lvl = h_lvl
        self.type = line_type
        self.shape = line_shape
        self.transition = [self._l_lvl, self._h_lvl]

    def verbose(self):
        print(f'{self.type} line from {self.element} with charge {self.charge} transitioning from {self._h_lvl} to {self._l_lvl} with {self.shape} shape.')

class line_of_sight:
    def __init__(self, start, target, wavelength_range, samples_per_task, bins_level, reflections = False, ref_level = 1):
        self.start            = start
        self.target           = target
        self.wavelength_range = wavelength_range
        self.samples_per_task = samples_per_task

        spectral_bins = [50, 500, 2000, 4000, 10000]
        self.spectral_bins = spectral_bins[bins_level]

        pixel_samples = [1, 50, 100, 500, 1000, 5000]
        self.reflections = reflections
        if reflections:
            self.pixel_samples = pixel_samples[ref_level]

        self.type = 'Line of sight'

    def verbose(self):
        ref_line = 'on' if self.reflections else 'off'
        print(f'Observer type {self.type} located at {self.start} and targeted at {self.target}, will compute from {self.wavelength_range[0]} to {self.wavelength_range[1]} nm, reflections are {ref_line}.')

class fibre_optic(line_of_sight):
    def __init__(self, start, target, wavelength_range, samples_per_task, bins_level, acceptance, radius, spectral_rays, reflections = False, ref_level = 1):
        super().__init__(start, target, wavelength_range, samples_per_task, bins_level, reflections = False, ref_level = 1)
        self.acceptance = acceptance
        self.radius = radius
        self.spectral_rays = spectral_rays
        self.type = 'Optical fiber'

class pinhole_camera(line_of_sight):
    def __init__(self, start, target, wavelength_range, samples_per_task, bins_level, fov, pixels, quality_level, reflections = False, ref_level = 1):
        super().__init__(start, target, wavelength_range, samples_per_task, bins_level, reflections = False, ref_level = 1)
        self.camera_fov = fov
        self.pixels = pixels

        min_samples = [10, 50, 100, 500, 1000]
        self.min_samples = min_samples[quality_level]

        self.type = 'Pinhole camera'

class cherab_settings:
    def __init__(self, plasma = False, vessel = False, dnb = False, nbi = False):
        self.setup_status       = setup_status(plasma, vessel, dnb, nbi)
        self.plasma_files       = plasma_files()
        self.cad_models         = []
        self.transform_settings = transform_setting()
        self.dnb_settings       = beam_settings()
        self.nbi_settings       = []

        self.plasma_models = []
        self.nbi_models    = []
        self.dnb_models    = []
        self.observer      = None



    def verbose(self):

        print('PLASMA SETTINGS:')
        if self.setup_status._plasma:
            self.plasma_files.verbose()
        else:
            print('Files are not selected yet')

        if len(self.plasma_models) > 0:
            for line in self.plasma_models:
                line.verbose()

        print('')
        print('VESSEL SETTINGS:')
        if self.setup_status._vessel:
            for model in self.cad_models:
                model.verbose()
        elif self.setup_status._vessel is None:
            print('Setting vessel is skipped')
        else:
            print('Vessel is not set up yet')

        print('')
        print('DNB SETTINGS:')
        if self.setup_status._dnb:
            self.dnb_settings.verbose()
            if len(self.dnb_models) > 0:
                for line in self.dnb_models:
                    line.verbose()

        elif self.setup_status._dnb is None:
            print('Setting DNB is skipped')
        else:
            print('DNB is not set up yet')

        print('')
        print('NBI SETTINGS:')
        if self.setup_status._nbi:
            for beam in self.nbi_settings:
                beam.verbose()
            if len(self.nbi_models) > 0:
                for line in self.nbi_models:
                    line.verbose()

        elif self.setup_status._nbi is None:
            print('Setting NBI is skipped')
        else:
            print('NBI is not set up yet')

        if not self.observer is None:
            self.observer.verbose()


