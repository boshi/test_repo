from PySide6.QtWidgets import QWidget, QMessageBox, QHBoxLayout, QLabel, QDoubleSpinBox, QSpinBox, QSpacerItem, QSizePolicy

def show_warning(text):
    dlg = QMessageBox()
    dlg.setText(text)
    dlg.exec()

def inline_widget(label_text : str, spinbox_type : bool = False, spinbox_value = 0, spinbox_range : list = [0, 1], spinbox_decimals : int = 2):

    layoutWidget = QWidget()
    layout       = QHBoxLayout(layoutWidget)

    layout.setContentsMargins(0, 0, 0, 0)

    lable = QLabel(layoutWidget)
    lable.setText(label_text)

    horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

    if spinbox_type:
        spinbox = QDoubleSpinBox(layoutWidget)
        spinbox.setDecimals(spinbox_decimals)
    else:
        spinbox = QSpinBox(layoutWidget)

    spinbox.setMinimum(spinbox_range[0])
    spinbox.setMaximum(spinbox_range[-1])

    spinbox.setValue(spinbox_value)

    layout.addWidget(lable)
    layout.addItem(horizontalSpacer)
    layout.addWidget(spinbox)

    return layoutWidget
