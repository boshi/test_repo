import platform

def system_check(func):
    def wrapper(*args, **kwargs):
        if platform.system() == 'Linux':
            return func(*args, **kwargs)
        else:
            print("Method can only be executed on Linux system")
    return wrapper