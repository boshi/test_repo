import numpy as np
from scipy.interpolate import interp1d

def neutralization_efficiensy(energy):
		# Efficiency of neutralisation in percent
		filename            = r'modules/neutralization.dat'
		neutralization_data = np.loadtxt(filename)
		beam_energy         = neutralization_data[:,0]
		efficiensy          = neutralization_data[:,1] / 100
		efficiensy_int      = interp1d(beam_energy, efficiensy, kind='cubic', bounds_error=False, fill_value=0.25)

		if energy > 1.e4:
			print('Assuming the energy is in eV')
			return efficiensy_int(energy/1e3)
		else:
			return efficiensy_int(energy)
		
def pol2cart(rho, phi):
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return(x, y)