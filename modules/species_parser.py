def species_parser(filepath):
    impurities = []
    fractions = []

    with open(filepath, 'r') as f:
        main_gas = f.readline().rstrip().lower()
        for line in f:
            if not line in ['\n', '\r\n']:
                impurity, fraction = line.rstrip().split(' ')
                
                impurities.append(impurity)
                fractions.append(float(fraction))
            else:
                break
    
    return main_gas, impurities, fractions
