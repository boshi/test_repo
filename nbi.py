# This Python file uses the following encoding: utf-8
from PySide6 import QtCore
from PySide6.QtCore import Qt
from PySide6.QtWidgets import QWidget, QDialog, QLabel

from gui.ui_nbi_form import Ui_nbi_form

from modules.gui_modules import show_warning

from modules.classes import beam_settings
from modules.syscheck import system_check

from dnb import dnb

from copy import deepcopy as cp

class nbi(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = Ui_nbi_form()
        self.ui.setupUi(self)
        self.nbi_list  = []
        self.nbi_count = 0
        self.base_dir  = None
        self.nbi_form = dnb()
        self.beam_settings_buffer = beam_settings()

        self.update_headers()

        self.ui.add_beam_button.clicked.connect(self.add)
        self.ui.remove_last_button.clicked.connect(self.remove_last)
        self.ui.check_beams_creation.clicked.connect(self.check_creation_button_pushed)

    # Add model button pushed --------------------------------------------------
    @QtCore.Slot()
    def add(self):
        self.nbi_form.ui.apply_and_close.clicked.connect(self.nbi_form_accept_pushed)

        self.beam_settings_buffer.clear()

        if self.nbi_form.exec() == QDialog.Accepted:
            self.update_headers()

            number_title = QLabel(f'{self.nbi_count}')
            element      = QLabel(f'{self.beam_settings_buffer.element}')
            energy       = QLabel(f'{self.beam_settings_buffer.energy}')

            rounded_start = [round(coordinate, 3) for coordinate in self.beam_settings_buffer.start]
            start_str = f'{rounded_start}'
            start     = QLabel(start_str[1:-1])

            rounded_focus = [round(coordinate, 3) for coordinate in self.beam_settings_buffer.focus]
            focus_str = f'{rounded_focus}'
            focus     = QLabel(focus_str[1:-1])

            self.ui.gridLayout.removeItem(self.ui.verticalSpacer_2)

            self.ui.gridLayout.addWidget(number_title, self.nbi_count, 0, Qt.AlignLeft)
            self.ui.gridLayout.addWidget(element     , self.nbi_count, 1, Qt.AlignLeft)
            self.ui.gridLayout.addWidget(energy      , self.nbi_count, 2, Qt.AlignLeft)
            self.ui.gridLayout.addWidget(start       , self.nbi_count, 3, Qt.AlignRight)
            self.ui.gridLayout.addWidget(focus       , self.nbi_count, 4, Qt.AlignRight)

            self.ui.gridLayout.addItem(self.ui.verticalSpacer_2)


    def nbi_form_prepare(self):
        last_beam_parameters = self.nbi_list[-1]
        power = last_beam_parameters.power
        print(power)

        if power[0] > ((power[1] + power[2]) * 100):
            components = 1
            full_power = power[0]
        else:
            components = 3
            full_power = sum(power)

        self.nbi_form.ui.beam_element.setCurrentText(f'{last_beam_parameters.element}')
        self.nbi_form.ui.beam_energy.setValue(last_beam_parameters.energy)
        self.nbi_form.ui.beam_radius.setValue(last_beam_parameters.radius)
        self.nbi_form.ui.beam_power_type.setCurrentText('Atoms')
        self.nbi_form.ui.beam_power.setValue(full_power)
        self.nbi_form.ui.beam_length.setValue(last_beam_parameters.length)
        self.nbi_form.ui.beam_components.setCurrentText(f'{components}')
        self.nbi_form.ui.beam_divergence.setValue(last_beam_parameters.divergence)
        self.nbi_form.ui.beam_temperature.setValue(last_beam_parameters.temperature)


        self.nbi_form.ui.fraction_e0.setValue(power[0] / full_power)

        if components == 3:
            self.nbi_form.ui.fraction_e2.setValue(power[1] / full_power)
            self.nbi_form.ui.fraction_e3.setValue(power[2] / full_power)

        self.nbi_form.ui.beam_start_x.setValue(last_beam_parameters.start[0])
        self.nbi_form.ui.beam_start_y.setValue(last_beam_parameters.start[1])
        self.nbi_form.ui.beam_start_z.setValue(last_beam_parameters.start[2])

        self.nbi_form.ui.beam_focus_x.setValue(last_beam_parameters.focus[0])
        self.nbi_form.ui.beam_focus_y.setValue(last_beam_parameters.focus[1])
        self.nbi_form.ui.beam_focus_z.setValue(last_beam_parameters.focus[2])


    ## Accept and close button (NBI form) -------------------------------------------
    def nbi_form_accept_pushed(self):
        beam_settings_buffer = self.nbi_form.retreve_settings()
        self.beam_settings_buffer = cp(beam_settings_buffer)
        if beam_settings_buffer is None:
            self.nbi_form.reject()

        else:
            self.nbi_form.accept()
            self.nbi_form.destroy()
            self.nbi_form = dnb()

            if not beam_settings_buffer.element is None:
                self.nbi_count += 1
                self.nbi_list.append(beam_settings_buffer)

            if len(self.nbi_list) > 0:
                self.nbi_form_prepare()


        # Common function
    def update_headers(self):
        if self.nbi_count > 0:
            state = False
        else:
            state = True

        self.ui.label_element.setHidden(state)
        self.ui.label_number_title.setHidden(state)
        self.ui.label_energy.setHidden(state)
        self.ui.label_start.setHidden(state)
        self.ui.label_focus.setHidden(state)


    # Remove last model button pushed ------------------------------------------
    @QtCore.Slot()
    def remove_last(self):
        if self.nbi_count > 0:
            self.nbi_count -= 1
            self.update_headers()
            self.remove_row()
            self.nbi_list.pop(-1)
            print(len(self.nbi_list))


    def remove_row(self):
        columns = self.ui.gridLayout.columnCount()
        row = self.nbi_count + 1
        for column in range(columns):
            item = self.ui.gridLayout.itemAtPosition(row, column)
            if item is not None:
                item.widget().deleteLater()
                self.ui.gridLayout.removeItem(item)


# Check beam creation button pushed --------------------------------------------
    @QtCore.Slot()
    def check_creation_button_pushed(self):
        counter = 0
        try:
            for beam in self.nbi_list:
                counter += 1
                self.nbi_form.check_beam_creation(beam, False)
            show_warning('All beam creation is ok')
        except:
            show_warning('Something is wrong with beam {counter}')