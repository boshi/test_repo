# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'form.ui'
##
## Created by: Qt User Interface Compiler version 6.6.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QAction, QBrush, QColor, QConicalGradient,
    QCursor, QFont, QFontDatabase, QGradient,
    QIcon, QImage, QKeySequence, QLinearGradient,
    QPainter, QPalette, QPixmap, QRadialGradient,
    QTransform)
from PySide6.QtWidgets import (QApplication, QCheckBox, QFrame, QGridLayout,
    QHBoxLayout, QLabel, QLayout, QMainWindow,
    QMenu, QMenuBar, QPushButton, QSizePolicy,
    QStatusBar, QWidget)

class Ui_Cherab_main(object):
    def setupUi(self, Cherab_main):
        if not Cherab_main.objectName():
            Cherab_main.setObjectName(u"Cherab_main")
        Cherab_main.resize(559, 364)
        self.actionRaysect_Documentation = QAction(Cherab_main)
        self.actionRaysect_Documentation.setObjectName(u"actionRaysect_Documentation")
        self.actionCherab_Documentation = QAction(Cherab_main)
        self.actionCherab_Documentation.setObjectName(u"actionCherab_Documentation")
        self.actionAbout = QAction(Cherab_main)
        self.actionAbout.setObjectName(u"actionAbout")
        self.actionSave_current_settings = QAction(Cherab_main)
        self.actionSave_current_settings.setObjectName(u"actionSave_current_settings")
        self.actionLoad_saved_settings = QAction(Cherab_main)
        self.actionLoad_saved_settings.setObjectName(u"actionLoad_saved_settings")
        self.actionConvert_Geometry = QAction(Cherab_main)
        self.actionConvert_Geometry.setObjectName(u"actionConvert_Geometry")
        self.actionCamera_Convoluton = QAction(Cherab_main)
        self.actionCamera_Convoluton.setObjectName(u"actionCamera_Convoluton")
        self.actionOptics_Test = QAction(Cherab_main)
        self.actionOptics_Test.setObjectName(u"actionOptics_Test")
        self.centralwidget = QWidget(Cherab_main)
        self.centralwidget.setObjectName(u"centralwidget")
        self.label_2 = QLabel(self.centralwidget)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setGeometry(QRect(10, 150, 211, 171))
        self.label_2.setTextFormat(Qt.RichText)
        self.label_2.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignTop)
        self.label_2.setWordWrap(True)
        self.gridLayoutWidget = QWidget(self.centralwidget)
        self.gridLayoutWidget.setObjectName(u"gridLayoutWidget")
        self.gridLayoutWidget.setGeometry(QRect(260, 10, 271, 241))
        self.gridLayout = QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setObjectName(u"gridLayout")
        self.gridLayout.setSizeConstraint(QLayout.SetMinimumSize)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.check_nbi = QCheckBox(self.gridLayoutWidget)
        self.check_nbi.setObjectName(u"check_nbi")
        self.check_nbi.setEnabled(False)

        self.gridLayout.addWidget(self.check_nbi, 4, 0, 1, 1)

        self.check_vessel = QCheckBox(self.gridLayoutWidget)
        self.check_vessel.setObjectName(u"check_vessel")
        self.check_vessel.setEnabled(False)
        self.check_vessel.setCheckable(True)

        self.gridLayout.addWidget(self.check_vessel, 2, 0, 1, 1)

        self.open_dnb = QPushButton(self.gridLayoutWidget)
        self.open_dnb.setObjectName(u"open_dnb")

        self.gridLayout.addWidget(self.open_dnb, 3, 1, 1, 1)

        self.open_nbi = QPushButton(self.gridLayoutWidget)
        self.open_nbi.setObjectName(u"open_nbi")

        self.gridLayout.addWidget(self.open_nbi, 4, 1, 1, 1)

        self.skip_dnb = QPushButton(self.gridLayoutWidget)
        self.skip_dnb.setObjectName(u"skip_dnb")

        self.gridLayout.addWidget(self.skip_dnb, 3, 2, 1, 1)

        self.skip_vessel = QPushButton(self.gridLayoutWidget)
        self.skip_vessel.setObjectName(u"skip_vessel")

        self.gridLayout.addWidget(self.skip_vessel, 2, 2, 1, 1)

        self.skip_nbi = QPushButton(self.gridLayoutWidget)
        self.skip_nbi.setObjectName(u"skip_nbi")

        self.gridLayout.addWidget(self.skip_nbi, 4, 2, 1, 1)

        self.open_emission = QPushButton(self.gridLayoutWidget)
        self.open_emission.setObjectName(u"open_emission")

        self.gridLayout.addWidget(self.open_emission, 5, 1, 1, 1)

        self.skip_plasma = QPushButton(self.gridLayoutWidget)
        self.skip_plasma.setObjectName(u"skip_plasma")
        self.skip_plasma.setEnabled(False)

        self.gridLayout.addWidget(self.skip_plasma, 1, 2, 1, 1)

        self.check_dnb = QCheckBox(self.gridLayoutWidget)
        self.check_dnb.setObjectName(u"check_dnb")
        self.check_dnb.setEnabled(False)
        self.check_dnb.setCheckable(True)

        self.gridLayout.addWidget(self.check_dnb, 3, 0, 1, 1)

        self.open_plasma = QPushButton(self.gridLayoutWidget)
        self.open_plasma.setObjectName(u"open_plasma")

        self.gridLayout.addWidget(self.open_plasma, 1, 1, 1, 1)

        self.check_plasma = QCheckBox(self.gridLayoutWidget)
        self.check_plasma.setObjectName(u"check_plasma")
        self.check_plasma.setEnabled(False)
        self.check_plasma.setCheckable(True)
        self.check_plasma.setChecked(False)

        self.gridLayout.addWidget(self.check_plasma, 1, 0, 1, 1)

        self.open_vessel = QPushButton(self.gridLayoutWidget)
        self.open_vessel.setObjectName(u"open_vessel")

        self.gridLayout.addWidget(self.open_vessel, 2, 1, 1, 1)

        self.skip_emission = QPushButton(self.gridLayoutWidget)
        self.skip_emission.setObjectName(u"skip_emission")
        self.skip_emission.setEnabled(False)

        self.gridLayout.addWidget(self.skip_emission, 5, 2, 1, 1)

        self.check_emission = QCheckBox(self.gridLayoutWidget)
        self.check_emission.setObjectName(u"check_emission")
        self.check_emission.setEnabled(False)
        self.check_emission.setCheckable(True)
        self.check_emission.setChecked(False)

        self.gridLayout.addWidget(self.check_emission, 5, 0, 1, 1)

        self.label = QLabel(self.centralwidget)
        self.label.setObjectName(u"label")
        self.label.setGeometry(QRect(10, 10, 71, 91))
        self.label.setPixmap(QPixmap(u"gui/KI_logo_150.png"))
        self.label.setScaledContents(True)
        self.horizontalLayoutWidget = QWidget(self.centralwidget)
        self.horizontalLayoutWidget.setObjectName(u"horizontalLayoutWidget")
        self.horizontalLayoutWidget.setGeometry(QRect(260, 280, 271, 31))
        self.horizontalLayout = QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.dump_script_button = QPushButton(self.horizontalLayoutWidget)
        self.dump_script_button.setObjectName(u"dump_script_button")

        self.horizontalLayout.addWidget(self.dump_script_button)

        self.start_simulation_button = QPushButton(self.horizontalLayoutWidget)
        self.start_simulation_button.setObjectName(u"start_simulation_button")

        self.horizontalLayout.addWidget(self.start_simulation_button)

        self.line = QFrame(self.centralwidget)
        self.line.setObjectName(u"line")
        self.line.setGeometry(QRect(260, 250, 271, 20))
        self.line.setFrameShape(QFrame.HLine)
        self.line.setFrameShadow(QFrame.Sunken)
        Cherab_main.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(Cherab_main)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 559, 21))
        self.menuCherab_MSE_generation = QMenu(self.menubar)
        self.menuCherab_MSE_generation.setObjectName(u"menuCherab_MSE_generation")
        self.menuOutput = QMenu(self.menubar)
        self.menuOutput.setObjectName(u"menuOutput")
        self.menuUtils = QMenu(self.menubar)
        self.menuUtils.setObjectName(u"menuUtils")
        Cherab_main.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(Cherab_main)
        self.statusbar.setObjectName(u"statusbar")
        Cherab_main.setStatusBar(self.statusbar)

        self.menubar.addAction(self.menuCherab_MSE_generation.menuAction())
        self.menubar.addAction(self.menuUtils.menuAction())
        self.menubar.addAction(self.menuOutput.menuAction())
        self.menuCherab_MSE_generation.addAction(self.actionSave_current_settings)
        self.menuCherab_MSE_generation.addAction(self.actionLoad_saved_settings)
        self.menuOutput.addAction(self.actionRaysect_Documentation)
        self.menuOutput.addAction(self.actionCherab_Documentation)
        self.menuOutput.addSeparator()
        self.menuOutput.addAction(self.actionAbout)
        self.menuUtils.addAction(self.actionConvert_Geometry)
        self.menuUtils.addAction(self.actionCamera_Convoluton)
        self.menuUtils.addAction(self.actionOptics_Test)

        self.retranslateUi(Cherab_main)

        QMetaObject.connectSlotsByName(Cherab_main)
    # setupUi

    def retranslateUi(self, Cherab_main):
        Cherab_main.setWindowTitle(QCoreApplication.translate("Cherab_main", u"Cherab MSE", None))
        self.actionRaysect_Documentation.setText(QCoreApplication.translate("Cherab_main", u"Raysect Documentation", None))
        self.actionCherab_Documentation.setText(QCoreApplication.translate("Cherab_main", u"Cherab Documentation", None))
        self.actionAbout.setText(QCoreApplication.translate("Cherab_main", u"About...", None))
        self.actionSave_current_settings.setText(QCoreApplication.translate("Cherab_main", u"Save current settings", None))
        self.actionLoad_saved_settings.setText(QCoreApplication.translate("Cherab_main", u"Load saved settings", None))
        self.actionConvert_Geometry.setText(QCoreApplication.translate("Cherab_main", u"Convert Geometry", None))
        self.actionCamera_Convoluton.setText(QCoreApplication.translate("Cherab_main", u"Camera Convoluton", None))
        self.actionOptics_Test.setText(QCoreApplication.translate("Cherab_main", u"Optics Test", None))
        self.label_2.setText(QCoreApplication.translate("Cherab_main", u"<html><head/><body><p>A GUI Application for Active slectral diagnostics Spectra generation using Cherab and Raysect.</p><p>This App follows a modular architecture. Please setup your plasma and diagnostic beam. Additionally you can add CAD models for vacuum vessel and setup heating beams.</p></body></html>", None))
        self.check_nbi.setText("")
        self.check_vessel.setText("")
        self.open_dnb.setText(QCoreApplication.translate("Cherab_main", u"Diagnostic beams Creation", None))
        self.open_nbi.setText(QCoreApplication.translate("Cherab_main", u"Heating beams Creation", None))
        self.skip_dnb.setText(QCoreApplication.translate("Cherab_main", u"Skip", None))
        self.skip_vessel.setText(QCoreApplication.translate("Cherab_main", u"Skip", None))
        self.skip_nbi.setText(QCoreApplication.translate("Cherab_main", u"Skip", None))
        self.open_emission.setText(QCoreApplication.translate("Cherab_main", u"Emission/Observation", None))
        self.skip_plasma.setText(QCoreApplication.translate("Cherab_main", u"Clear", None))
        self.check_dnb.setText("")
        self.open_plasma.setText(QCoreApplication.translate("Cherab_main", u"Plasma Setup", None))
        self.check_plasma.setText("")
        self.open_vessel.setText(QCoreApplication.translate("Cherab_main", u"Vessel Setup", None))
        self.skip_emission.setText(QCoreApplication.translate("Cherab_main", u"Clear", None))
        self.check_emission.setText("")
        self.label.setText("")
        self.dump_script_button.setText(QCoreApplication.translate("Cherab_main", u"Dump Script", None))
        self.start_simulation_button.setText(QCoreApplication.translate("Cherab_main", u"Start Simulation", None))
        self.menuCherab_MSE_generation.setTitle(QCoreApplication.translate("Cherab_main", u"File", None))
        self.menuOutput.setTitle(QCoreApplication.translate("Cherab_main", u"About", None))
        self.menuUtils.setTitle(QCoreApplication.translate("Cherab_main", u"Utils", None))
    # retranslateUi

