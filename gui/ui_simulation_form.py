# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'simulation_form.ui'
##
## Created by: Qt User Interface Compiler version 6.6.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QCheckBox, QComboBox, QDoubleSpinBox,
    QFrame, QHBoxLayout, QLabel, QLayout,
    QPushButton, QScrollArea, QSizePolicy, QSpacerItem,
    QSpinBox, QTextEdit, QVBoxLayout, QWidget)

class Ui_simulation_form(object):
    def setupUi(self, simulation_form):
        if not simulation_form.objectName():
            simulation_form.setObjectName(u"simulation_form")
        simulation_form.resize(894, 800)
        self.line = QFrame(simulation_form)
        self.line.setObjectName(u"line")
        self.line.setGeometry(QRect(550, 10, 20, 781))
        self.line.setFrameShape(QFrame.VLine)
        self.line.setFrameShadow(QFrame.Sunken)
        self.label_title_model = QLabel(simulation_form)
        self.label_title_model.setObjectName(u"label_title_model")
        self.label_title_model.setGeometry(QRect(10, 0, 341, 41))
        font = QFont()
        font.setPointSize(14)
        self.label_title_model.setFont(font)
        self.label_title_observer = QLabel(simulation_form)
        self.label_title_observer.setObjectName(u"label_title_observer")
        self.label_title_observer.setGeometry(QRect(540, 0, 341, 41))
        self.label_title_observer.setFont(font)
        self.label_title_observer.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
        self.label_warning = QLabel(simulation_form)
        self.label_warning.setObjectName(u"label_warning")
        self.label_warning.setGeometry(QRect(10, 30, 541, 61))
        self.button_accept_emission = QPushButton(simulation_form)
        self.button_accept_emission.setObjectName(u"button_accept_emission")
        self.button_accept_emission.setGeometry(QRect(770, 770, 111, 24))
        self.horizontalLayoutWidget_2 = QWidget(simulation_form)
        self.horizontalLayoutWidget_2.setObjectName(u"horizontalLayoutWidget_2")
        self.horizontalLayoutWidget_2.setGeometry(QRect(0, 100, 551, 31))
        self.hlayout_title_plasma_emission = QHBoxLayout(self.horizontalLayoutWidget_2)
        self.hlayout_title_plasma_emission.setObjectName(u"hlayout_title_plasma_emission")
        self.hlayout_title_plasma_emission.setContentsMargins(0, 0, 0, 0)
        self.label_plasma_emission = QLabel(self.horizontalLayoutWidget_2)
        self.label_plasma_emission.setObjectName(u"label_plasma_emission")

        self.hlayout_title_plasma_emission.addWidget(self.label_plasma_emission)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.hlayout_title_plasma_emission.addItem(self.horizontalSpacer)

        self.check_bremsstralung = QCheckBox(self.horizontalLayoutWidget_2)
        self.check_bremsstralung.setObjectName(u"check_bremsstralung")

        self.hlayout_title_plasma_emission.addWidget(self.check_bremsstralung)

        self.button_add_plasma_line = QPushButton(self.horizontalLayoutWidget_2)
        self.button_add_plasma_line.setObjectName(u"button_add_plasma_line")

        self.hlayout_title_plasma_emission.addWidget(self.button_add_plasma_line)

        self.button_remove_plasma_line = QPushButton(self.horizontalLayoutWidget_2)
        self.button_remove_plasma_line.setObjectName(u"button_remove_plasma_line")

        self.hlayout_title_plasma_emission.addWidget(self.button_remove_plasma_line)

        self.horizontalLayoutWidget_3 = QWidget(simulation_form)
        self.horizontalLayoutWidget_3.setObjectName(u"horizontalLayoutWidget_3")
        self.horizontalLayoutWidget_3.setGeometry(QRect(0, 410, 551, 31))
        self.hlayout_title_dnb_emission = QHBoxLayout(self.horizontalLayoutWidget_3)
        self.hlayout_title_dnb_emission.setObjectName(u"hlayout_title_dnb_emission")
        self.hlayout_title_dnb_emission.setContentsMargins(0, 0, 0, 0)
        self.label_dnb_model = QLabel(self.horizontalLayoutWidget_3)
        self.label_dnb_model.setObjectName(u"label_dnb_model")

        self.hlayout_title_dnb_emission.addWidget(self.label_dnb_model)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.hlayout_title_dnb_emission.addItem(self.horizontalSpacer_2)

        self.button_add_dnb_line = QPushButton(self.horizontalLayoutWidget_3)
        self.button_add_dnb_line.setObjectName(u"button_add_dnb_line")

        self.hlayout_title_dnb_emission.addWidget(self.button_add_dnb_line)

        self.button_remove_dnb_line = QPushButton(self.horizontalLayoutWidget_3)
        self.button_remove_dnb_line.setObjectName(u"button_remove_dnb_line")

        self.hlayout_title_dnb_emission.addWidget(self.button_remove_dnb_line)

        self.horizontalLayoutWidget_4 = QWidget(simulation_form)
        self.horizontalLayoutWidget_4.setObjectName(u"horizontalLayoutWidget_4")
        self.horizontalLayoutWidget_4.setGeometry(QRect(0, 600, 551, 31))
        self.hlayout_title_nbi_emission = QHBoxLayout(self.horizontalLayoutWidget_4)
        self.hlayout_title_nbi_emission.setObjectName(u"hlayout_title_nbi_emission")
        self.hlayout_title_nbi_emission.setContentsMargins(0, 0, 0, 0)
        self.label_nbi_model = QLabel(self.horizontalLayoutWidget_4)
        self.label_nbi_model.setObjectName(u"label_nbi_model")

        self.hlayout_title_nbi_emission.addWidget(self.label_nbi_model)

        self.horizontalSpacer_3 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.hlayout_title_nbi_emission.addItem(self.horizontalSpacer_3)

        self.button_add_nbi_line = QPushButton(self.horizontalLayoutWidget_4)
        self.button_add_nbi_line.setObjectName(u"button_add_nbi_line")

        self.hlayout_title_nbi_emission.addWidget(self.button_add_nbi_line)

        self.button_remove_nbi_line = QPushButton(self.horizontalLayoutWidget_4)
        self.button_remove_nbi_line.setObjectName(u"button_remove_nbi_line")

        self.hlayout_title_nbi_emission.addWidget(self.button_remove_nbi_line)

        self.scrollArea_plasma = QScrollArea(simulation_form)
        self.scrollArea_plasma.setObjectName(u"scrollArea_plasma")
        self.scrollArea_plasma.setGeometry(QRect(0, 160, 551, 231))
        self.scrollArea_plasma.setWidgetResizable(True)
        self.scrollAreaWidgetContents = QWidget()
        self.scrollAreaWidgetContents.setObjectName(u"scrollAreaWidgetContents")
        self.scrollAreaWidgetContents.setGeometry(QRect(0, 0, 549, 229))
        self.scrollArea_plasma.setWidget(self.scrollAreaWidgetContents)
        self.scrollArea_dnb = QScrollArea(simulation_form)
        self.scrollArea_dnb.setObjectName(u"scrollArea_dnb")
        self.scrollArea_dnb.setGeometry(QRect(0, 460, 551, 131))
        self.scrollArea_dnb.setWidgetResizable(True)
        self.scrollAreaWidgetContents_2 = QWidget()
        self.scrollAreaWidgetContents_2.setObjectName(u"scrollAreaWidgetContents_2")
        self.scrollAreaWidgetContents_2.setGeometry(QRect(0, 0, 549, 129))
        self.scrollArea_dnb.setWidget(self.scrollAreaWidgetContents_2)
        self.scrollArea_nbi = QScrollArea(simulation_form)
        self.scrollArea_nbi.setObjectName(u"scrollArea_nbi")
        self.scrollArea_nbi.setGeometry(QRect(0, 660, 551, 131))
        self.scrollArea_nbi.setWidgetResizable(True)
        self.scrollAreaWidgetContents_3 = QWidget()
        self.scrollAreaWidgetContents_3.setObjectName(u"scrollAreaWidgetContents_3")
        self.scrollAreaWidgetContents_3.setGeometry(QRect(0, 0, 549, 129))
        self.scrollArea_nbi.setWidget(self.scrollAreaWidgetContents_3)
        self.check_lines_button = QPushButton(simulation_form)
        self.check_lines_button.setObjectName(u"check_lines_button")
        self.check_lines_button.setGeometry(QRect(570, 770, 81, 24))
        self.label = QLabel(simulation_form)
        self.label.setObjectName(u"label")
        self.label.setGeometry(QRect(10, 140, 16, 16))
        self.label_2 = QLabel(simulation_form)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setGeometry(QRect(20, 140, 61, 20))
        self.label_2.setAlignment(Qt.AlignCenter)
        self.label_3 = QLabel(simulation_form)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setGeometry(QRect(80, 140, 51, 20))
        self.label_3.setAlignment(Qt.AlignCenter)
        self.label_4 = QLabel(simulation_form)
        self.label_4.setObjectName(u"label_4")
        self.label_4.setGeometry(QRect(140, 140, 121, 20))
        self.label_4.setAlignment(Qt.AlignCenter)
        self.label_5 = QLabel(simulation_form)
        self.label_5.setObjectName(u"label_5")
        self.label_5.setGeometry(QRect(250, 140, 141, 20))
        self.label_5.setAlignment(Qt.AlignCenter)
        self.label_6 = QLabel(simulation_form)
        self.label_6.setObjectName(u"label_6")
        self.label_6.setGeometry(QRect(390, 140, 161, 20))
        self.label_6.setAlignment(Qt.AlignCenter)
        self.label_7 = QLabel(simulation_form)
        self.label_7.setObjectName(u"label_7")
        self.label_7.setGeometry(QRect(10, 440, 16, 16))
        self.label_8 = QLabel(simulation_form)
        self.label_8.setObjectName(u"label_8")
        self.label_8.setGeometry(QRect(30, 440, 81, 16))
        self.label_8.setAlignment(Qt.AlignCenter)
        self.label_9 = QLabel(simulation_form)
        self.label_9.setObjectName(u"label_9")
        self.label_9.setGeometry(QRect(240, 440, 311, 20))
        self.label_9.setAlignment(Qt.AlignCenter)
        self.label_14 = QLabel(simulation_form)
        self.label_14.setObjectName(u"label_14")
        self.label_14.setGeometry(QRect(110, 440, 131, 20))
        self.label_14.setAlignment(Qt.AlignCenter)
        self.label_15 = QLabel(simulation_form)
        self.label_15.setObjectName(u"label_15")
        self.label_15.setGeometry(QRect(110, 640, 131, 20))
        self.label_15.setAlignment(Qt.AlignCenter)
        self.label_10 = QLabel(simulation_form)
        self.label_10.setObjectName(u"label_10")
        self.label_10.setGeometry(QRect(240, 640, 311, 20))
        self.label_10.setAlignment(Qt.AlignCenter)
        self.label_11 = QLabel(simulation_form)
        self.label_11.setObjectName(u"label_11")
        self.label_11.setGeometry(QRect(10, 640, 16, 16))
        self.label_12 = QLabel(simulation_form)
        self.label_12.setObjectName(u"label_12")
        self.label_12.setGeometry(QRect(30, 640, 81, 16))
        self.label_12.setAlignment(Qt.AlignCenter)
        self.verticalLayoutWidget = QWidget(simulation_form)
        self.verticalLayoutWidget.setObjectName(u"verticalLayoutWidget")
        self.verticalLayoutWidget.setGeometry(QRect(570, 40, 311, 241))
        self.vlayout_observer_common = QVBoxLayout(self.verticalLayoutWidget)
        self.vlayout_observer_common.setObjectName(u"vlayout_observer_common")
        self.vlayout_observer_common.setContentsMargins(0, 0, 0, 0)
        self.label_13 = QLabel(self.verticalLayoutWidget)
        self.label_13.setObjectName(u"label_13")

        self.vlayout_observer_common.addWidget(self.label_13)

        self.hlayout_observer_start = QHBoxLayout()
        self.hlayout_observer_start.setObjectName(u"hlayout_observer_start")
        self.move_x_label = QLabel(self.verticalLayoutWidget)
        self.move_x_label.setObjectName(u"move_x_label")

        self.hlayout_observer_start.addWidget(self.move_x_label)

        self.observer_start_x = QDoubleSpinBox(self.verticalLayoutWidget)
        self.observer_start_x.setObjectName(u"observer_start_x")
        self.observer_start_x.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
        self.observer_start_x.setMinimum(-100.000000000000000)
        self.observer_start_x.setMaximum(100.000000000000000)
        self.observer_start_x.setSingleStep(0.010000000000000)

        self.hlayout_observer_start.addWidget(self.observer_start_x)

        self.move_y_label = QLabel(self.verticalLayoutWidget)
        self.move_y_label.setObjectName(u"move_y_label")

        self.hlayout_observer_start.addWidget(self.move_y_label)

        self.observer_start_y = QDoubleSpinBox(self.verticalLayoutWidget)
        self.observer_start_y.setObjectName(u"observer_start_y")
        self.observer_start_y.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
        self.observer_start_y.setMinimum(-100.000000000000000)
        self.observer_start_y.setMaximum(100.000000000000000)
        self.observer_start_y.setSingleStep(0.010000000000000)

        self.hlayout_observer_start.addWidget(self.observer_start_y)

        self.move_z_label = QLabel(self.verticalLayoutWidget)
        self.move_z_label.setObjectName(u"move_z_label")

        self.hlayout_observer_start.addWidget(self.move_z_label)

        self.observer_start_z = QDoubleSpinBox(self.verticalLayoutWidget)
        self.observer_start_z.setObjectName(u"observer_start_z")
        self.observer_start_z.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
        self.observer_start_z.setMinimum(-100.000000000000000)
        self.observer_start_z.setMaximum(100.000000000000000)
        self.observer_start_z.setSingleStep(0.010000000000000)

        self.hlayout_observer_start.addWidget(self.observer_start_z)


        self.vlayout_observer_common.addLayout(self.hlayout_observer_start)

        self.label_16 = QLabel(self.verticalLayoutWidget)
        self.label_16.setObjectName(u"label_16")

        self.vlayout_observer_common.addWidget(self.label_16)

        self.hlayout_observer_target = QHBoxLayout()
        self.hlayout_observer_target.setObjectName(u"hlayout_observer_target")
        self.move_x_label_2 = QLabel(self.verticalLayoutWidget)
        self.move_x_label_2.setObjectName(u"move_x_label_2")

        self.hlayout_observer_target.addWidget(self.move_x_label_2)

        self.observer_target_x = QDoubleSpinBox(self.verticalLayoutWidget)
        self.observer_target_x.setObjectName(u"observer_target_x")
        self.observer_target_x.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
        self.observer_target_x.setMinimum(-100.000000000000000)
        self.observer_target_x.setMaximum(100.000000000000000)
        self.observer_target_x.setSingleStep(0.010000000000000)

        self.hlayout_observer_target.addWidget(self.observer_target_x)

        self.move_y_label_2 = QLabel(self.verticalLayoutWidget)
        self.move_y_label_2.setObjectName(u"move_y_label_2")

        self.hlayout_observer_target.addWidget(self.move_y_label_2)

        self.observer_target_y = QDoubleSpinBox(self.verticalLayoutWidget)
        self.observer_target_y.setObjectName(u"observer_target_y")
        self.observer_target_y.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
        self.observer_target_y.setMinimum(-100.000000000000000)
        self.observer_target_y.setMaximum(100.000000000000000)
        self.observer_target_y.setSingleStep(0.010000000000000)

        self.hlayout_observer_target.addWidget(self.observer_target_y)

        self.move_z_label_2 = QLabel(self.verticalLayoutWidget)
        self.move_z_label_2.setObjectName(u"move_z_label_2")

        self.hlayout_observer_target.addWidget(self.move_z_label_2)

        self.observer_target_z = QDoubleSpinBox(self.verticalLayoutWidget)
        self.observer_target_z.setObjectName(u"observer_target_z")
        self.observer_target_z.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
        self.observer_target_z.setMinimum(-100.000000000000000)
        self.observer_target_z.setMaximum(100.000000000000000)
        self.observer_target_z.setSingleStep(0.010000000000000)

        self.hlayout_observer_target.addWidget(self.observer_target_z)


        self.vlayout_observer_common.addLayout(self.hlayout_observer_target)

        self.hlayout_wavelength_range = QHBoxLayout()
        self.hlayout_wavelength_range.setObjectName(u"hlayout_wavelength_range")
        self.label_17 = QLabel(self.verticalLayoutWidget)
        self.label_17.setObjectName(u"label_17")

        self.hlayout_wavelength_range.addWidget(self.label_17)

        self.horizontalSpacer_6 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.hlayout_wavelength_range.addItem(self.horizontalSpacer_6)

        self.min_wl_spin = QDoubleSpinBox(self.verticalLayoutWidget)
        self.min_wl_spin.setObjectName(u"min_wl_spin")
        self.min_wl_spin.setMinimum(0.000000000000000)
        self.min_wl_spin.setMaximum(1000.000000000000000)
        self.min_wl_spin.setValue(380.000000000000000)

        self.hlayout_wavelength_range.addWidget(self.min_wl_spin)

        self.max_wl_spin = QDoubleSpinBox(self.verticalLayoutWidget)
        self.max_wl_spin.setObjectName(u"max_wl_spin")
        self.max_wl_spin.setMaximum(1000.000000000000000)
        self.max_wl_spin.setValue(780.000000000000000)

        self.hlayout_wavelength_range.addWidget(self.max_wl_spin)


        self.vlayout_observer_common.addLayout(self.hlayout_wavelength_range)

        self.hlayout_samples_per_task = QHBoxLayout()
        self.hlayout_samples_per_task.setObjectName(u"hlayout_samples_per_task")
        self.label_18 = QLabel(self.verticalLayoutWidget)
        self.label_18.setObjectName(u"label_18")

        self.hlayout_samples_per_task.addWidget(self.label_18)

        self.horizontalSpacer_4 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.hlayout_samples_per_task.addItem(self.horizontalSpacer_4)

        self.samples_per_task_spin = QSpinBox(self.verticalLayoutWidget)
        self.samples_per_task_spin.setObjectName(u"samples_per_task_spin")
        self.samples_per_task_spin.setMinimum(1)
        self.samples_per_task_spin.setMaximum(10000)

        self.hlayout_samples_per_task.addWidget(self.samples_per_task_spin)


        self.vlayout_observer_common.addLayout(self.hlayout_samples_per_task)

        self.hlayout_spectral_sampling = QHBoxLayout()
        self.hlayout_spectral_sampling.setObjectName(u"hlayout_spectral_sampling")
        self.label_20 = QLabel(self.verticalLayoutWidget)
        self.label_20.setObjectName(u"label_20")

        self.hlayout_spectral_sampling.addWidget(self.label_20)

        self.horizontalSpacer_5 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.hlayout_spectral_sampling.addItem(self.horizontalSpacer_5)

        self.sampling_level = QSpinBox(self.verticalLayoutWidget)
        self.sampling_level.setObjectName(u"sampling_level")
        self.sampling_level.setMinimum(1)
        self.sampling_level.setMaximum(5)

        self.hlayout_spectral_sampling.addWidget(self.sampling_level)


        self.vlayout_observer_common.addLayout(self.hlayout_spectral_sampling)

        self.hlayout_observer_selection = QHBoxLayout()
        self.hlayout_observer_selection.setObjectName(u"hlayout_observer_selection")
        self.label_observer_select = QLabel(self.verticalLayoutWidget)
        self.label_observer_select.setObjectName(u"label_observer_select")

        self.hlayout_observer_selection.addWidget(self.label_observer_select)

        self.comboBox_observer_select = QComboBox(self.verticalLayoutWidget)
        self.comboBox_observer_select.addItem("")
        self.comboBox_observer_select.addItem("")
        self.comboBox_observer_select.addItem("")
        self.comboBox_observer_select.setObjectName(u"comboBox_observer_select")

        self.hlayout_observer_selection.addWidget(self.comboBox_observer_select)


        self.vlayout_observer_common.addLayout(self.hlayout_observer_selection)

        self.verticalLayoutWidget_2 = QWidget(simulation_form)
        self.verticalLayoutWidget_2.setObjectName(u"verticalLayoutWidget_2")
        self.verticalLayoutWidget_2.setGeometry(QRect(570, 430, 311, 61))
        self.vlayout_reflections = QVBoxLayout(self.verticalLayoutWidget_2)
        self.vlayout_reflections.setObjectName(u"vlayout_reflections")
        self.vlayout_reflections.setContentsMargins(0, 0, 0, 0)
        self.check_reflections = QCheckBox(self.verticalLayoutWidget_2)
        self.check_reflections.setObjectName(u"check_reflections")

        self.vlayout_reflections.addWidget(self.check_reflections)

        self.hlayout_reflections_level = QHBoxLayout()
        self.hlayout_reflections_level.setObjectName(u"hlayout_reflections_level")
        self.label_19 = QLabel(self.verticalLayoutWidget_2)
        self.label_19.setObjectName(u"label_19")

        self.hlayout_reflections_level.addWidget(self.label_19)

        self.horizontalSpacer_7 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.hlayout_reflections_level.addItem(self.horizontalSpacer_7)

        self.pixel_samples_level = QSpinBox(self.verticalLayoutWidget_2)
        self.pixel_samples_level.setObjectName(u"pixel_samples_level")
        self.pixel_samples_level.setEnabled(False)
        self.pixel_samples_level.setMinimum(1)
        self.pixel_samples_level.setMaximum(6)

        self.hlayout_reflections_level.addWidget(self.pixel_samples_level)


        self.vlayout_reflections.addLayout(self.hlayout_reflections_level)

        self.msg_output = QTextEdit(simulation_form)
        self.msg_output.setObjectName(u"msg_output")
        self.msg_output.setGeometry(QRect(570, 520, 311, 241))
        self.msg_output.setReadOnly(True)
        self.label_21 = QLabel(simulation_form)
        self.label_21.setObjectName(u"label_21")
        self.label_21.setGeometry(QRect(570, 500, 311, 20))
        self.verticalLayoutWidget_3 = QWidget(simulation_form)
        self.verticalLayoutWidget_3.setObjectName(u"verticalLayoutWidget_3")
        self.verticalLayoutWidget_3.setGeometry(QRect(570, 290, 311, 131))
        self.observer_specific_parameters = QVBoxLayout(self.verticalLayoutWidget_3)
        self.observer_specific_parameters.setObjectName(u"observer_specific_parameters")
        self.observer_specific_parameters.setSizeConstraint(QLayout.SetDefaultConstraint)
        self.observer_specific_parameters.setContentsMargins(0, 0, 0, 0)
        self.observer_extra_spacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.observer_specific_parameters.addItem(self.observer_extra_spacer)

        self.check_observer_button = QPushButton(simulation_form)
        self.check_observer_button.setObjectName(u"check_observer_button")
        self.check_observer_button.setGeometry(QRect(660, 770, 101, 24))

        self.retranslateUi(simulation_form)

        QMetaObject.connectSlotsByName(simulation_form)
    # setupUi

    def retranslateUi(self, simulation_form):
        simulation_form.setWindowTitle(QCoreApplication.translate("simulation_form", u"Form", None))
        self.label_title_model.setText(QCoreApplication.translate("simulation_form", u"Set Emission Models", None))
        self.label_title_observer.setText(QCoreApplication.translate("simulation_form", u"Set Observer", None))
        self.label_warning.setText(QCoreApplication.translate("simulation_form", u"<html><head/><body><p><span style=\" font-size:10pt;\">Note that the App </span><span style=\" font-size:10pt; font-weight:700;\">does not check </span><span style=\" font-size:10pt;\">if the Line you selected exists or<br/>if the data needed for calculations is present in ADAS.<br/>All faulty lines are </span><span style=\" font-size:10pt; font-weight:700;\">skipped</span><span style=\" font-size:10pt;\"> during the simulation.</span></p></body></html>", None))
        self.button_accept_emission.setText(QCoreApplication.translate("simulation_form", u"Accept and close", None))
        self.label_plasma_emission.setText(QCoreApplication.translate("simulation_form", u"<html><head/><body><p><span style=\" font-weight:700;\">Plasma Emission</span></p></body></html>", None))
        self.check_bremsstralung.setText(QCoreApplication.translate("simulation_form", u"Bremsstrahlung", None))
        self.button_add_plasma_line.setText(QCoreApplication.translate("simulation_form", u"Add Line", None))
        self.button_remove_plasma_line.setText(QCoreApplication.translate("simulation_form", u"Remove Last Line", None))
        self.label_dnb_model.setText(QCoreApplication.translate("simulation_form", u"<html><head/><body><p><span style=\" font-weight:700;\">Diagnostic Beam Emission</span></p></body></html>", None))
        self.button_add_dnb_line.setText(QCoreApplication.translate("simulation_form", u"Add Line", None))
        self.button_remove_dnb_line.setText(QCoreApplication.translate("simulation_form", u"Remove Last Line", None))
        self.label_nbi_model.setText(QCoreApplication.translate("simulation_form", u"<html><head/><body><p><span style=\" font-weight:700;\">Heating Beam Emission</span></p></body></html>", None))
        self.button_add_nbi_line.setText(QCoreApplication.translate("simulation_form", u"Add Line", None))
        self.button_remove_nbi_line.setText(QCoreApplication.translate("simulation_form", u"Remove Last Line", None))
        self.check_lines_button.setText(QCoreApplication.translate("simulation_form", u"Check Lines", None))
        self.label.setText(QCoreApplication.translate("simulation_form", u"#", None))
        self.label_2.setText(QCoreApplication.translate("simulation_form", u"Element", None))
        self.label_3.setText(QCoreApplication.translate("simulation_form", u"Charge", None))
        self.label_4.setText(QCoreApplication.translate("simulation_form", u"Transition", None))
        self.label_5.setText(QCoreApplication.translate("simulation_form", u"Type", None))
        self.label_6.setText(QCoreApplication.translate("simulation_form", u"Shape", None))
        self.label_7.setText(QCoreApplication.translate("simulation_form", u"#", None))
        self.label_8.setText(QCoreApplication.translate("simulation_form", u"Type", None))
        self.label_9.setText(QCoreApplication.translate("simulation_form", u"Shape Arguments", None))
        self.label_14.setText(QCoreApplication.translate("simulation_form", u"Transition", None))
        self.label_15.setText(QCoreApplication.translate("simulation_form", u"Transition", None))
        self.label_10.setText(QCoreApplication.translate("simulation_form", u"Shape Arguments", None))
        self.label_11.setText(QCoreApplication.translate("simulation_form", u"#", None))
        self.label_12.setText(QCoreApplication.translate("simulation_form", u"Type", None))
        self.label_13.setText(QCoreApplication.translate("simulation_form", u"Observer position:", None))
        self.move_x_label.setText(QCoreApplication.translate("simulation_form", u"X:", None))
        self.move_y_label.setText(QCoreApplication.translate("simulation_form", u"Y:", None))
        self.move_z_label.setText(QCoreApplication.translate("simulation_form", u"Z:", None))
        self.label_16.setText(QCoreApplication.translate("simulation_form", u"Observer target:", None))
        self.move_x_label_2.setText(QCoreApplication.translate("simulation_form", u"X:", None))
        self.move_y_label_2.setText(QCoreApplication.translate("simulation_form", u"Y:", None))
        self.move_z_label_2.setText(QCoreApplication.translate("simulation_form", u"Z:", None))
        self.label_17.setText(QCoreApplication.translate("simulation_form", u"Wavelength Range:", None))
        self.label_18.setText(QCoreApplication.translate("simulation_form", u"Samples per task", None))
        self.label_20.setText(QCoreApplication.translate("simulation_form", u"Spectral Sampling Level", None))
        self.label_observer_select.setText(QCoreApplication.translate("simulation_form", u"Observer type", None))
        self.comboBox_observer_select.setItemText(0, QCoreApplication.translate("simulation_form", u"Line Of Sight", None))
        self.comboBox_observer_select.setItemText(1, QCoreApplication.translate("simulation_form", u"Optical Fiber", None))
        self.comboBox_observer_select.setItemText(2, QCoreApplication.translate("simulation_form", u"Pinhole Camera", None))

        self.check_reflections.setText(QCoreApplication.translate("simulation_form", u"Reflections from Vessel", None))
        self.label_19.setText(QCoreApplication.translate("simulation_form", u"Reflection Sampling Level", None))
        self.label_21.setText(QCoreApplication.translate("simulation_form", u"Console Output", None))
        self.check_observer_button.setText(QCoreApplication.translate("simulation_form", u"Check Observer", None))
    # retranslateUi

