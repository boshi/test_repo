# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'dnb_form.ui'
##
## Created by: Qt User Interface Compiler version 6.6.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QComboBox, QDoubleSpinBox, QFormLayout,
    QFrame, QGridLayout, QHBoxLayout, QLabel,
    QPushButton, QSizePolicy, QSpacerItem, QVBoxLayout,
    QWidget)

class Ui_dnb_form(object):
    def setupUi(self, dnb_form):
        if not dnb_form.objectName():
            dnb_form.setObjectName(u"dnb_form")
        dnb_form.resize(302, 601)
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(dnb_form.sizePolicy().hasHeightForWidth())
        dnb_form.setSizePolicy(sizePolicy)
        self.label_title = QLabel(dnb_form)
        self.label_title.setObjectName(u"label_title")
        self.label_title.setGeometry(QRect(10, 0, 271, 31))
        font = QFont()
        font.setPointSize(14)
        self.label_title.setFont(font)
        self.label_title.setAlignment(Qt.AlignCenter)
        self.verticalLayoutWidget_2 = QWidget(dnb_form)
        self.verticalLayoutWidget_2.setObjectName(u"verticalLayoutWidget_2")
        self.verticalLayoutWidget_2.setGeometry(QRect(10, 30, 281, 564))
        self.beam_general_vertical_layout = QVBoxLayout(self.verticalLayoutWidget_2)
        self.beam_general_vertical_layout.setObjectName(u"beam_general_vertical_layout")
        self.beam_general_vertical_layout.setContentsMargins(0, 0, 0, 0)
        self.line = QFrame(self.verticalLayoutWidget_2)
        self.line.setObjectName(u"line")
        self.line.setFrameShape(QFrame.HLine)
        self.line.setFrameShadow(QFrame.Sunken)

        self.beam_general_vertical_layout.addWidget(self.line)

        self.formLayout = QFormLayout()
        self.formLayout.setObjectName(u"formLayout")
        self.label_beam_element = QLabel(self.verticalLayoutWidget_2)
        self.label_beam_element.setObjectName(u"label_beam_element")

        self.formLayout.setWidget(0, QFormLayout.LabelRole, self.label_beam_element)

        self.beam_element = QComboBox(self.verticalLayoutWidget_2)
        self.beam_element.addItem("")
        self.beam_element.addItem("")
        self.beam_element.setObjectName(u"beam_element")
        self.beam_element.setMaximumSize(QSize(200, 16777215))
        self.beam_element.setLayoutDirection(Qt.RightToLeft)

        self.formLayout.setWidget(0, QFormLayout.FieldRole, self.beam_element)

        self.label_beam_energy = QLabel(self.verticalLayoutWidget_2)
        self.label_beam_energy.setObjectName(u"label_beam_energy")

        self.formLayout.setWidget(1, QFormLayout.LabelRole, self.label_beam_energy)

        self.beam_energy = QDoubleSpinBox(self.verticalLayoutWidget_2)
        self.beam_energy.setObjectName(u"beam_energy")
        self.beam_energy.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
        self.beam_energy.setMinimum(0.010000000000000)
        self.beam_energy.setMaximum(10000.000000000000000)
        self.beam_energy.setValue(60.000000000000000)

        self.formLayout.setWidget(1, QFormLayout.FieldRole, self.beam_energy)

        self.label_beam_radius = QLabel(self.verticalLayoutWidget_2)
        self.label_beam_radius.setObjectName(u"label_beam_radius")

        self.formLayout.setWidget(2, QFormLayout.LabelRole, self.label_beam_radius)

        self.beam_radius = QDoubleSpinBox(self.verticalLayoutWidget_2)
        self.beam_radius.setObjectName(u"beam_radius")
        self.beam_radius.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
        self.beam_radius.setDecimals(3)
        self.beam_radius.setMinimum(0.001000000000000)
        self.beam_radius.setMaximum(10000.000000000000000)
        self.beam_radius.setSingleStep(0.010000000000000)
        self.beam_radius.setValue(0.100000000000000)

        self.formLayout.setWidget(2, QFormLayout.FieldRole, self.beam_radius)

        self.label_beam_power_type = QLabel(self.verticalLayoutWidget_2)
        self.label_beam_power_type.setObjectName(u"label_beam_power_type")

        self.formLayout.setWidget(3, QFormLayout.LabelRole, self.label_beam_power_type)

        self.beam_power_type = QComboBox(self.verticalLayoutWidget_2)
        self.beam_power_type.addItem("")
        self.beam_power_type.addItem("")
        self.beam_power_type.setObjectName(u"beam_power_type")
        self.beam_power_type.setLayoutDirection(Qt.RightToLeft)

        self.formLayout.setWidget(3, QFormLayout.FieldRole, self.beam_power_type)

        self.label_beam_power = QLabel(self.verticalLayoutWidget_2)
        self.label_beam_power.setObjectName(u"label_beam_power")

        self.formLayout.setWidget(4, QFormLayout.LabelRole, self.label_beam_power)

        self.beam_power = QDoubleSpinBox(self.verticalLayoutWidget_2)
        self.beam_power.setObjectName(u"beam_power")
        self.beam_power.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
        self.beam_power.setMinimum(0.010000000000000)
        self.beam_power.setMaximum(10000.000000000000000)
        self.beam_power.setValue(300.000000000000000)

        self.formLayout.setWidget(4, QFormLayout.FieldRole, self.beam_power)

        self.label_beam_length = QLabel(self.verticalLayoutWidget_2)
        self.label_beam_length.setObjectName(u"label_beam_length")

        self.formLayout.setWidget(5, QFormLayout.LabelRole, self.label_beam_length)

        self.beam_length = QDoubleSpinBox(self.verticalLayoutWidget_2)
        self.beam_length.setObjectName(u"beam_length")
        self.beam_length.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
        self.beam_length.setDecimals(3)
        self.beam_length.setMinimum(0.001000000000000)
        self.beam_length.setMaximum(10000.000000000000000)
        self.beam_length.setSingleStep(0.010000000000000)
        self.beam_length.setValue(5.000000000000000)

        self.formLayout.setWidget(5, QFormLayout.FieldRole, self.beam_length)

        self.label_beam_components = QLabel(self.verticalLayoutWidget_2)
        self.label_beam_components.setObjectName(u"label_beam_components")

        self.formLayout.setWidget(6, QFormLayout.LabelRole, self.label_beam_components)

        self.beam_components = QComboBox(self.verticalLayoutWidget_2)
        self.beam_components.addItem("")
        self.beam_components.addItem("")
        self.beam_components.setObjectName(u"beam_components")
        self.beam_components.setLayoutDirection(Qt.RightToLeft)

        self.formLayout.setWidget(6, QFormLayout.FieldRole, self.beam_components)

        self.label_beam_divergence = QLabel(self.verticalLayoutWidget_2)
        self.label_beam_divergence.setObjectName(u"label_beam_divergence")

        self.formLayout.setWidget(7, QFormLayout.LabelRole, self.label_beam_divergence)

        self.beam_divergence = QDoubleSpinBox(self.verticalLayoutWidget_2)
        self.beam_divergence.setObjectName(u"beam_divergence")
        self.beam_divergence.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
        self.beam_divergence.setDecimals(3)
        self.beam_divergence.setMinimum(0.000000000000000)
        self.beam_divergence.setMaximum(360.000000000000000)
        self.beam_divergence.setSingleStep(0.010000000000000)
        self.beam_divergence.setValue(0.100000000000000)

        self.formLayout.setWidget(7, QFormLayout.FieldRole, self.beam_divergence)

        self.label_beam_temperature = QLabel(self.verticalLayoutWidget_2)
        self.label_beam_temperature.setObjectName(u"label_beam_temperature")

        self.formLayout.setWidget(8, QFormLayout.LabelRole, self.label_beam_temperature)

        self.beam_temperature = QDoubleSpinBox(self.verticalLayoutWidget_2)
        self.beam_temperature.setObjectName(u"beam_temperature")
        self.beam_temperature.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
        self.beam_temperature.setDecimals(1)
        self.beam_temperature.setMinimum(0.100000000000000)
        self.beam_temperature.setMaximum(10000.000000000000000)
        self.beam_temperature.setValue(10.000000000000000)

        self.formLayout.setWidget(8, QFormLayout.FieldRole, self.beam_temperature)


        self.beam_general_vertical_layout.addLayout(self.formLayout)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.beam_general_vertical_layout.addItem(self.verticalSpacer)

        self.line_4 = QFrame(self.verticalLayoutWidget_2)
        self.line_4.setObjectName(u"line_4")
        self.line_4.setFrameShape(QFrame.HLine)
        self.line_4.setFrameShadow(QFrame.Sunken)

        self.beam_general_vertical_layout.addWidget(self.line_4)

        self.label_beam_composition = QLabel(self.verticalLayoutWidget_2)
        self.label_beam_composition.setObjectName(u"label_beam_composition")

        self.beam_general_vertical_layout.addWidget(self.label_beam_composition)

        self.beam_composition_horisontal_layout = QHBoxLayout()
        self.beam_composition_horisontal_layout.setObjectName(u"beam_composition_horisontal_layout")
        self.label_e0 = QLabel(self.verticalLayoutWidget_2)
        self.label_e0.setObjectName(u"label_e0")

        self.beam_composition_horisontal_layout.addWidget(self.label_e0)

        self.fraction_e0 = QDoubleSpinBox(self.verticalLayoutWidget_2)
        self.fraction_e0.setObjectName(u"fraction_e0")
        self.fraction_e0.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
        self.fraction_e0.setDecimals(2)
        self.fraction_e0.setMinimum(0.000000000000000)
        self.fraction_e0.setMaximum(1.000000000000000)
        self.fraction_e0.setSingleStep(0.010000000000000)
        self.fraction_e0.setValue(1.000000000000000)

        self.beam_composition_horisontal_layout.addWidget(self.fraction_e0)

        self.label_e2 = QLabel(self.verticalLayoutWidget_2)
        self.label_e2.setObjectName(u"label_e2")

        self.beam_composition_horisontal_layout.addWidget(self.label_e2)

        self.fraction_e2 = QDoubleSpinBox(self.verticalLayoutWidget_2)
        self.fraction_e2.setObjectName(u"fraction_e2")
        self.fraction_e2.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
        self.fraction_e2.setDecimals(2)
        self.fraction_e2.setMinimum(0.000000000000000)
        self.fraction_e2.setMaximum(1.000000000000000)
        self.fraction_e2.setSingleStep(0.010000000000000)
        self.fraction_e2.setValue(0.000000000000000)

        self.beam_composition_horisontal_layout.addWidget(self.fraction_e2)

        self.label_e3 = QLabel(self.verticalLayoutWidget_2)
        self.label_e3.setObjectName(u"label_e3")

        self.beam_composition_horisontal_layout.addWidget(self.label_e3)

        self.fraction_e3 = QDoubleSpinBox(self.verticalLayoutWidget_2)
        self.fraction_e3.setObjectName(u"fraction_e3")
        self.fraction_e3.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
        self.fraction_e3.setDecimals(2)
        self.fraction_e3.setMinimum(0.000000000000000)
        self.fraction_e3.setMaximum(1.000000000000000)
        self.fraction_e3.setSingleStep(0.010000000000000)
        self.fraction_e3.setValue(0.000000000000000)

        self.beam_composition_horisontal_layout.addWidget(self.fraction_e3)


        self.beam_general_vertical_layout.addLayout(self.beam_composition_horisontal_layout)

        self.verticalSpacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.beam_general_vertical_layout.addItem(self.verticalSpacer_2)

        self.line_2 = QFrame(self.verticalLayoutWidget_2)
        self.line_2.setObjectName(u"line_2")
        self.line_2.setFrameShape(QFrame.HLine)
        self.line_2.setFrameShadow(QFrame.Sunken)

        self.beam_general_vertical_layout.addWidget(self.line_2)

        self.gridLayout_3 = QGridLayout()
        self.gridLayout_3.setObjectName(u"gridLayout_3")
        self.gridLayout_2 = QGridLayout()
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.label_beam_focus_y = QLabel(self.verticalLayoutWidget_2)
        self.label_beam_focus_y.setObjectName(u"label_beam_focus_y")

        self.gridLayout_2.addWidget(self.label_beam_focus_y, 1, 0, 1, 1)

        self.beam_focus_y = QDoubleSpinBox(self.verticalLayoutWidget_2)
        self.beam_focus_y.setObjectName(u"beam_focus_y")
        self.beam_focus_y.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
        self.beam_focus_y.setDecimals(3)
        self.beam_focus_y.setMinimum(-100.000000000000000)
        self.beam_focus_y.setMaximum(100.000000000000000)
        self.beam_focus_y.setSingleStep(0.010000000000000)
        self.beam_focus_y.setValue(0.000000000000000)

        self.gridLayout_2.addWidget(self.beam_focus_y, 1, 1, 1, 1)

        self.label_beam_focus_x = QLabel(self.verticalLayoutWidget_2)
        self.label_beam_focus_x.setObjectName(u"label_beam_focus_x")

        self.gridLayout_2.addWidget(self.label_beam_focus_x, 0, 0, 1, 1)

        self.beam_focus_x = QDoubleSpinBox(self.verticalLayoutWidget_2)
        self.beam_focus_x.setObjectName(u"beam_focus_x")
        self.beam_focus_x.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
        self.beam_focus_x.setDecimals(3)
        self.beam_focus_x.setMinimum(-100.000000000000000)
        self.beam_focus_x.setMaximum(100.000000000000000)
        self.beam_focus_x.setSingleStep(0.010000000000000)
        self.beam_focus_x.setValue(0.000000000000000)

        self.gridLayout_2.addWidget(self.beam_focus_x, 0, 1, 1, 1)

        self.label_beam_focus_z = QLabel(self.verticalLayoutWidget_2)
        self.label_beam_focus_z.setObjectName(u"label_beam_focus_z")

        self.gridLayout_2.addWidget(self.label_beam_focus_z, 2, 0, 1, 1)

        self.beam_focus_z = QDoubleSpinBox(self.verticalLayoutWidget_2)
        self.beam_focus_z.setObjectName(u"beam_focus_z")
        self.beam_focus_z.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
        self.beam_focus_z.setDecimals(3)
        self.beam_focus_z.setMinimum(-100.000000000000000)
        self.beam_focus_z.setMaximum(100.000000000000000)
        self.beam_focus_z.setSingleStep(0.010000000000000)
        self.beam_focus_z.setValue(0.000000000000000)

        self.gridLayout_2.addWidget(self.beam_focus_z, 2, 1, 1, 1)


        self.gridLayout_3.addLayout(self.gridLayout_2, 1, 2, 1, 1)

        self.label_beam_focus = QLabel(self.verticalLayoutWidget_2)
        self.label_beam_focus.setObjectName(u"label_beam_focus")
        self.label_beam_focus.setAlignment(Qt.AlignCenter)

        self.gridLayout_3.addWidget(self.label_beam_focus, 0, 2, 1, 1)

        self.label_beam_start = QLabel(self.verticalLayoutWidget_2)
        self.label_beam_start.setObjectName(u"label_beam_start")
        self.label_beam_start.setAlignment(Qt.AlignCenter)

        self.gridLayout_3.addWidget(self.label_beam_start, 0, 0, 1, 1)

        self.gridLayout = QGridLayout()
        self.gridLayout.setObjectName(u"gridLayout")
        self.beam_start_x = QDoubleSpinBox(self.verticalLayoutWidget_2)
        self.beam_start_x.setObjectName(u"beam_start_x")
        self.beam_start_x.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
        self.beam_start_x.setDecimals(3)
        self.beam_start_x.setMinimum(-100.000000000000000)
        self.beam_start_x.setMaximum(100.000000000000000)
        self.beam_start_x.setSingleStep(0.010000000000000)
        self.beam_start_x.setValue(5.000000000000000)

        self.gridLayout.addWidget(self.beam_start_x, 0, 1, 1, 1)

        self.label_beam_start_x = QLabel(self.verticalLayoutWidget_2)
        self.label_beam_start_x.setObjectName(u"label_beam_start_x")

        self.gridLayout.addWidget(self.label_beam_start_x, 0, 0, 1, 1)

        self.beam_start_y = QDoubleSpinBox(self.verticalLayoutWidget_2)
        self.beam_start_y.setObjectName(u"beam_start_y")
        self.beam_start_y.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
        self.beam_start_y.setDecimals(3)
        self.beam_start_y.setMinimum(-100.000000000000000)
        self.beam_start_y.setMaximum(100.000000000000000)
        self.beam_start_y.setSingleStep(0.010000000000000)
        self.beam_start_y.setValue(0.000000000000000)

        self.gridLayout.addWidget(self.beam_start_y, 1, 1, 1, 1)

        self.label_beam_start_y = QLabel(self.verticalLayoutWidget_2)
        self.label_beam_start_y.setObjectName(u"label_beam_start_y")

        self.gridLayout.addWidget(self.label_beam_start_y, 1, 0, 1, 1)

        self.label_beam_start_z = QLabel(self.verticalLayoutWidget_2)
        self.label_beam_start_z.setObjectName(u"label_beam_start_z")

        self.gridLayout.addWidget(self.label_beam_start_z, 2, 0, 1, 1)

        self.beam_start_z = QDoubleSpinBox(self.verticalLayoutWidget_2)
        self.beam_start_z.setObjectName(u"beam_start_z")
        self.beam_start_z.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
        self.beam_start_z.setDecimals(3)
        self.beam_start_z.setMinimum(-100.000000000000000)
        self.beam_start_z.setMaximum(100.000000000000000)
        self.beam_start_z.setSingleStep(0.010000000000000)
        self.beam_start_z.setValue(0.000000000000000)

        self.gridLayout.addWidget(self.beam_start_z, 2, 1, 1, 1)


        self.gridLayout_3.addLayout(self.gridLayout, 1, 0, 1, 1)

        self.line_5 = QFrame(self.verticalLayoutWidget_2)
        self.line_5.setObjectName(u"line_5")
        self.line_5.setFrameShape(QFrame.VLine)
        self.line_5.setFrameShadow(QFrame.Sunken)

        self.gridLayout_3.addWidget(self.line_5, 1, 1, 1, 1)


        self.beam_general_vertical_layout.addLayout(self.gridLayout_3)

        self.line_3 = QFrame(self.verticalLayoutWidget_2)
        self.line_3.setObjectName(u"line_3")
        self.line_3.setFrameShape(QFrame.HLine)
        self.line_3.setFrameShadow(QFrame.Sunken)

        self.beam_general_vertical_layout.addWidget(self.line_3)

        self.verticalSpacer_3 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.beam_general_vertical_layout.addItem(self.verticalSpacer_3)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.check_beam_creation = QPushButton(self.verticalLayoutWidget_2)
        self.check_beam_creation.setObjectName(u"check_beam_creation")

        self.horizontalLayout.addWidget(self.check_beam_creation)


        self.beam_general_vertical_layout.addLayout(self.horizontalLayout)

        self.apply_and_close = QPushButton(self.verticalLayoutWidget_2)
        self.apply_and_close.setObjectName(u"apply_and_close")

        self.beam_general_vertical_layout.addWidget(self.apply_and_close)


        self.retranslateUi(dnb_form)

        QMetaObject.connectSlotsByName(dnb_form)
    # setupUi

    def retranslateUi(self, dnb_form):
        dnb_form.setWindowTitle(QCoreApplication.translate("dnb_form", u"Beam Parameters", None))
        self.label_title.setText(QCoreApplication.translate("dnb_form", u"Define DNB parameters", None))
        self.label_beam_element.setText(QCoreApplication.translate("dnb_form", u"Beam Species", None))
        self.beam_element.setItemText(0, QCoreApplication.translate("dnb_form", u"hydrogen", None))
        self.beam_element.setItemText(1, QCoreApplication.translate("dnb_form", u"deuterium", None))

        self.label_beam_energy.setText(QCoreApplication.translate("dnb_form", u"Main energy, keV", None))
        self.label_beam_radius.setText(QCoreApplication.translate("dnb_form", u"Beam radius in focus, m", None))
        self.label_beam_power_type.setText(QCoreApplication.translate("dnb_form", u"Power is set for:", None))
        self.beam_power_type.setItemText(0, QCoreApplication.translate("dnb_form", u"Atoms", None))
        self.beam_power_type.setItemText(1, QCoreApplication.translate("dnb_form", u"Ions", None))

        self.label_beam_power.setText(QCoreApplication.translate("dnb_form", u"Beam power, kW", None))
        self.label_beam_length.setText(QCoreApplication.translate("dnb_form", u"Beam length, m", None))
        self.label_beam_components.setText(QCoreApplication.translate("dnb_form", u"Number of energy components", None))
        self.beam_components.setItemText(0, QCoreApplication.translate("dnb_form", u"1", None))
        self.beam_components.setItemText(1, QCoreApplication.translate("dnb_form", u"3", None))

        self.label_beam_divergence.setText(QCoreApplication.translate("dnb_form", u"Beam Divergence, deg", None))
        self.label_beam_temperature.setText(QCoreApplication.translate("dnb_form", u"Beam \"Temperture\", eV", None))
        self.label_beam_composition.setText(QCoreApplication.translate("dnb_form", u"Beam Composition (in Atoms)", None))
        self.label_e0.setText(QCoreApplication.translate("dnb_form", u"<html><head/><body><p>E<span style=\" vertical-align:sub;\">0</span>:</p></body></html>", None))
        self.label_e2.setText(QCoreApplication.translate("dnb_form", u"<html><head/><body><p>E<span style=\" vertical-align:sub;\">0</span>/2:</p></body></html>", None))
        self.label_e3.setText(QCoreApplication.translate("dnb_form", u"<html><head/><body><p>E<span style=\" vertical-align:sub;\">0</span>/3:</p></body></html>", None))
        self.label_beam_focus_y.setText(QCoreApplication.translate("dnb_form", u"Y, m:", None))
        self.label_beam_focus_x.setText(QCoreApplication.translate("dnb_form", u"X, m:", None))
        self.label_beam_focus_z.setText(QCoreApplication.translate("dnb_form", u"Z, m:", None))
        self.label_beam_focus.setText(QCoreApplication.translate("dnb_form", u"Beam Focal Point", None))
        self.label_beam_start.setText(QCoreApplication.translate("dnb_form", u"Beam Starting Point", None))
        self.label_beam_start_x.setText(QCoreApplication.translate("dnb_form", u"X, m:", None))
        self.label_beam_start_y.setText(QCoreApplication.translate("dnb_form", u"Y, m:", None))
        self.label_beam_start_z.setText(QCoreApplication.translate("dnb_form", u"Z, m:", None))
        self.check_beam_creation.setText(QCoreApplication.translate("dnb_form", u"Check Beam Creation", None))
        self.apply_and_close.setText(QCoreApplication.translate("dnb_form", u"Apply and close", None))
    # retranslateUi

