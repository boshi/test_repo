# This Python file uses the following encoding: utf-8
from PySide6 import QtCore
from PySide6 import QtWidgets

# from ui_vessel_widget import Ui_Form

from PySide6.QtWidgets import (QComboBox, QDoubleSpinBox, QGridLayout,
    QLabel, QLineEdit)

class vessel_widget(QtWidgets.QWidget):
    def __init__(self, parent=None, number=0, line=''):
        super().__init__(parent)

        items = ['None', 'aluminium', 'beryllium', 'cobolt', 'copper', 'gold', 'graphite', 'iron', 'lithium', 'magnesium', 'mercury', 'nickel', 'palladium', 'platinum', 'silicon', 'silver', 'sodium', 'steel', 'titanium', 'tungsten']

        self.label_number_title = QLabel('Model #')
        self.label_roughness = QLabel("Roughness")
        self.label_material = QLabel("Material")
        self.label_path = QLabel("Path to model")
        
        self.label_number = QLabel(f'{number}')
        self.path_line = QLineEdit(line)
        self.material_dropdown = QComboBox(self)
        self.roughness_spin = QDoubleSpinBox(self)

        self.path_line.setMaximumWidth(420)

        self.material_dropdown.addItems(items)
        self.material_dropdown.setCurrentIndex(0)

        self.roughness_spin.setDecimals(2)
        self.roughness_spin.setSingleStep(0.01)
        self.roughness_spin.setMaximum(1)
        self.roughness_spin.setMinimum(0)

        self.gridLayout = QGridLayout(self)

        line_number = 0
        
        if number == 1:
            line_number = 1
            self.gridLayout.addWidget(self.label_roughness,    0, 3, 1, 1)
            self.gridLayout.addWidget(self.label_material,     0, 2, 1, 1)
            self.gridLayout.addWidget(self.label_path,         0, 1, 1, 1)
            self.gridLayout.addWidget(self.label_number_title, 0, 0, 1, 1)

        self.gridLayout.addWidget(self.label_number,      line_number, 0, 1, 1)
        self.gridLayout.addWidget(self.path_line,         line_number, 1, 1, 1)
        self.gridLayout.addWidget(self.material_dropdown, line_number, 2, 1, 1)
        self.gridLayout.addWidget(self.roughness_spin,    line_number, 3, 1, 1)