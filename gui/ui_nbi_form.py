# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'nbi_form.ui'
##
## Created by: Qt User Interface Compiler version 6.6.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QGridLayout, QLabel, QPushButton,
    QSizePolicy, QSpacerItem, QVBoxLayout, QWidget)

class Ui_nbi_form(object):
    def setupUi(self, nbi_form):
        if not nbi_form.objectName():
            nbi_form.setObjectName(u"nbi_form")
        nbi_form.resize(802, 402)
        self.label = QLabel(nbi_form)
        self.label.setObjectName(u"label")
        self.label.setGeometry(QRect(10, 0, 921, 26))
        font = QFont()
        font.setPointSize(14)
        self.label.setFont(font)
        self.verticalLayoutWidget_2 = QWidget(nbi_form)
        self.verticalLayoutWidget_2.setObjectName(u"verticalLayoutWidget_2")
        self.verticalLayoutWidget_2.setGeometry(QRect(635, 60, 161, 331))
        self.verticalLayout_2 = QVBoxLayout(self.verticalLayoutWidget_2)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.add_beam_button = QPushButton(self.verticalLayoutWidget_2)
        self.add_beam_button.setObjectName(u"add_beam_button")

        self.verticalLayout_2.addWidget(self.add_beam_button)

        self.remove_last_button = QPushButton(self.verticalLayoutWidget_2)
        self.remove_last_button.setObjectName(u"remove_last_button")

        self.verticalLayout_2.addWidget(self.remove_last_button)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_2.addItem(self.verticalSpacer)

        self.check_beams_creation = QPushButton(self.verticalLayoutWidget_2)
        self.check_beams_creation.setObjectName(u"check_beams_creation")

        self.verticalLayout_2.addWidget(self.check_beams_creation)

        self.accept_close_nbi = QPushButton(self.verticalLayoutWidget_2)
        self.accept_close_nbi.setObjectName(u"accept_close_nbi")

        self.verticalLayout_2.addWidget(self.accept_close_nbi)

        self.gridLayoutWidget = QWidget(nbi_form)
        self.gridLayoutWidget.setObjectName(u"gridLayoutWidget")
        self.gridLayoutWidget.setGeometry(QRect(10, 60, 611, 331))
        self.gridLayout = QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setObjectName(u"gridLayout")
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.label_element = QLabel(self.gridLayoutWidget)
        self.label_element.setObjectName(u"label_element")
        self.label_element.setEnabled(True)
        self.label_element.setMaximumSize(QSize(100, 16777215))

        self.gridLayout.addWidget(self.label_element, 0, 1, 1, 1)

        self.verticalSpacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout.addItem(self.verticalSpacer_2, 1, 2, 1, 1)

        self.label_number_title = QLabel(self.gridLayoutWidget)
        self.label_number_title.setObjectName(u"label_number_title")
        self.label_number_title.setMaximumSize(QSize(50, 16777215))

        self.gridLayout.addWidget(self.label_number_title, 0, 0, 1, 1)

        self.label_focus = QLabel(self.gridLayoutWidget)
        self.label_focus.setObjectName(u"label_focus")
        self.label_focus.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.label_focus, 0, 4, 1, 1)

        self.label_start = QLabel(self.gridLayoutWidget)
        self.label_start.setObjectName(u"label_start")
        self.label_start.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.label_start, 0, 3, 1, 1)

        self.label_energy = QLabel(self.gridLayoutWidget)
        self.label_energy.setObjectName(u"label_energy")
        self.label_energy.setMaximumSize(QSize(100, 16777215))

        self.gridLayout.addWidget(self.label_energy, 0, 2, 1, 1)


        self.retranslateUi(nbi_form)

        QMetaObject.connectSlotsByName(nbi_form)
    # setupUi

    def retranslateUi(self, nbi_form):
        nbi_form.setWindowTitle(QCoreApplication.translate("nbi_form", u"Form", None))
        self.label.setText(QCoreApplication.translate("nbi_form", u"Define parameters for all NBI ingectors", None))
        self.add_beam_button.setText(QCoreApplication.translate("nbi_form", u"Add Heating Beam", None))
        self.remove_last_button.setText(QCoreApplication.translate("nbi_form", u"Remove last Beam", None))
        self.check_beams_creation.setText(QCoreApplication.translate("nbi_form", u"Check All Beams Creation", None))
        self.accept_close_nbi.setText(QCoreApplication.translate("nbi_form", u"Accept and Close", None))
        self.label_element.setText(QCoreApplication.translate("nbi_form", u"<html><head/><body><p><span style=\" font-weight:700;\">Element</span></p></body></html>", None))
        self.label_number_title.setText(QCoreApplication.translate("nbi_form", u"<html><head/><body><p><span style=\" font-weight:700;\">Beam #</span></p></body></html>", None))
        self.label_focus.setText(QCoreApplication.translate("nbi_form", u"<html><head/><body><p><span style=\" font-weight:700;\">Focus, m</span></p></body></html>", None))
        self.label_start.setText(QCoreApplication.translate("nbi_form", u"<html><head/><body><p><span style=\" font-weight:700;\">Start, m</span></p></body></html>", None))
        self.label_energy.setText(QCoreApplication.translate("nbi_form", u"<html><head/><body><p><span style=\" font-weight:700; font-style:italic;\">E</span><span style=\" font-weight:700; vertical-align:sub;\">0</span><span style=\" font-weight:700;\">, keV</span></p></body></html>", None))
    # retranslateUi

