# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'vessel.ui'
##
## Created by: Qt User Interface Compiler version 6.6.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QHBoxLayout, QLabel, QLayout,
    QLineEdit, QPushButton, QSizePolicy, QSpacerItem,
    QVBoxLayout, QWidget)

class Ui_vessel_form(object):
    def setupUi(self, vessel_form):
        if not vessel_form.objectName():
            vessel_form.setObjectName(u"vessel_form")
        vessel_form.resize(963, 483)
        self.label = QLabel(vessel_form)
        self.label.setObjectName(u"label")
        self.label.setGeometry(QRect(10, 0, 921, 26))
        font = QFont()
        font.setPointSize(14)
        self.label.setFont(font)
        self.verticalLayoutWidget_2 = QWidget(vessel_form)
        self.verticalLayoutWidget_2.setObjectName(u"verticalLayoutWidget_2")
        self.verticalLayoutWidget_2.setGeometry(QRect(720, 70, 211, 401))
        self.verticalLayout_2 = QVBoxLayout(self.verticalLayoutWidget_2)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.verticalLayout_2.setSizeConstraint(QLayout.SetMinAndMaxSize)
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.add_model = QPushButton(self.verticalLayoutWidget_2)
        self.add_model.setObjectName(u"add_model")

        self.verticalLayout_2.addWidget(self.add_model)

        self.remove_model = QPushButton(self.verticalLayoutWidget_2)
        self.remove_model.setObjectName(u"remove_model")

        self.verticalLayout_2.addWidget(self.remove_model)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_2.addItem(self.verticalSpacer)

        self.label_3 = QLabel(self.verticalLayoutWidget_2)
        self.label_3.setObjectName(u"label_3")

        self.verticalLayout_2.addWidget(self.label_3)

        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.move_title_label = QLabel(self.verticalLayoutWidget_2)
        self.move_title_label.setObjectName(u"move_title_label")

        self.verticalLayout.addWidget(self.move_title_label)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.move_x_label = QLabel(self.verticalLayoutWidget_2)
        self.move_x_label.setObjectName(u"move_x_label")

        self.horizontalLayout_2.addWidget(self.move_x_label)

        self.move_x_edit = QLineEdit(self.verticalLayoutWidget_2)
        self.move_x_edit.setObjectName(u"move_x_edit")
        self.move_x_edit.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.horizontalLayout_2.addWidget(self.move_x_edit)

        self.move_y_label = QLabel(self.verticalLayoutWidget_2)
        self.move_y_label.setObjectName(u"move_y_label")

        self.horizontalLayout_2.addWidget(self.move_y_label)

        self.move_y_edit = QLineEdit(self.verticalLayoutWidget_2)
        self.move_y_edit.setObjectName(u"move_y_edit")
        self.move_y_edit.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.horizontalLayout_2.addWidget(self.move_y_edit)

        self.move_z_label = QLabel(self.verticalLayoutWidget_2)
        self.move_z_label.setObjectName(u"move_z_label")

        self.horizontalLayout_2.addWidget(self.move_z_label)

        self.move_z_edit = QLineEdit(self.verticalLayoutWidget_2)
        self.move_z_edit.setObjectName(u"move_z_edit")
        self.move_z_edit.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.horizontalLayout_2.addWidget(self.move_z_edit)


        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.rotate_title_label = QLabel(self.verticalLayoutWidget_2)
        self.rotate_title_label.setObjectName(u"rotate_title_label")

        self.verticalLayout.addWidget(self.rotate_title_label)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.rotate_x_label = QLabel(self.verticalLayoutWidget_2)
        self.rotate_x_label.setObjectName(u"rotate_x_label")

        self.horizontalLayout.addWidget(self.rotate_x_label)

        self.rotate_x_edit = QLineEdit(self.verticalLayoutWidget_2)
        self.rotate_x_edit.setObjectName(u"rotate_x_edit")
        self.rotate_x_edit.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.horizontalLayout.addWidget(self.rotate_x_edit)

        self.rotate_y_label = QLabel(self.verticalLayoutWidget_2)
        self.rotate_y_label.setObjectName(u"rotate_y_label")

        self.horizontalLayout.addWidget(self.rotate_y_label)

        self.rotate_y_edit = QLineEdit(self.verticalLayoutWidget_2)
        self.rotate_y_edit.setObjectName(u"rotate_y_edit")
        self.rotate_y_edit.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.horizontalLayout.addWidget(self.rotate_y_edit)

        self.rotate_z_label = QLabel(self.verticalLayoutWidget_2)
        self.rotate_z_label.setObjectName(u"rotate_z_label")

        self.horizontalLayout.addWidget(self.rotate_z_label)

        self.rotate_z_edit = QLineEdit(self.verticalLayoutWidget_2)
        self.rotate_z_edit.setObjectName(u"rotate_z_edit")
        self.rotate_z_edit.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.horizontalLayout.addWidget(self.rotate_z_edit)


        self.verticalLayout.addLayout(self.horizontalLayout)

        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.button_transform_accept = QPushButton(self.verticalLayoutWidget_2)
        self.button_transform_accept.setObjectName(u"button_transform_accept")

        self.horizontalLayout_3.addWidget(self.button_transform_accept)

        self.button_transform_clear = QPushButton(self.verticalLayoutWidget_2)
        self.button_transform_clear.setObjectName(u"button_transform_clear")

        self.horizontalLayout_3.addWidget(self.button_transform_clear)


        self.verticalLayout.addLayout(self.horizontalLayout_3)


        self.verticalLayout_2.addLayout(self.verticalLayout)

        self.verticalSpacer_3 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_2.addItem(self.verticalSpacer_3)

        self.check_vessel = QPushButton(self.verticalLayoutWidget_2)
        self.check_vessel.setObjectName(u"check_vessel")

        self.verticalLayout_2.addWidget(self.check_vessel)

        self.accept_exit = QPushButton(self.verticalLayoutWidget_2)
        self.accept_exit.setObjectName(u"accept_exit")

        self.verticalLayout_2.addWidget(self.accept_exit)

        self.verticalLayoutWidget = QWidget(vessel_form)
        self.verticalLayoutWidget.setObjectName(u"verticalLayoutWidget")
        self.verticalLayoutWidget.setGeometry(QRect(10, 70, 691, 401))
        self.verticalLayout_1 = QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout_1.setObjectName(u"verticalLayout_1")
        self.verticalLayout_1.setSizeConstraint(QLayout.SetMinAndMaxSize)
        self.verticalLayout_1.setContentsMargins(0, 0, 0, 0)
        self.verticalSpacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_1.addItem(self.verticalSpacer_2)

        self.label_2 = QLabel(vessel_form)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setGeometry(QRect(10, 20, 921, 51))
        self.label_2.setWordWrap(True)

        self.retranslateUi(vessel_form)

        QMetaObject.connectSlotsByName(vessel_form)
    # setupUi

    def retranslateUi(self, vessel_form):
        vessel_form.setWindowTitle(QCoreApplication.translate("vessel_form", u"CAD models setup module", None))
        self.label.setText(QCoreApplication.translate("vessel_form", u"Define vessel CAD models and their properties", None))
        self.add_model.setText(QCoreApplication.translate("vessel_form", u"Add Model", None))
        self.remove_model.setText(QCoreApplication.translate("vessel_form", u"Remove Last Model", None))
        self.label_3.setText(QCoreApplication.translate("vessel_form", u"<html><head/><body><p align=\"center\"><span style=\" font-size:11pt; font-weight:700;\">Setup Transform</span></p></body></html>", None))
        self.move_title_label.setText(QCoreApplication.translate("vessel_form", u"<html><head/><body><p>Move tha basis along the axis<br/>(in meters)</p></body></html>", None))
        self.move_x_label.setText(QCoreApplication.translate("vessel_form", u"X:", None))
        self.move_x_edit.setInputMask("")
        self.move_x_edit.setText(QCoreApplication.translate("vessel_form", u"0", None))
        self.move_y_label.setText(QCoreApplication.translate("vessel_form", u"Y:", None))
        self.move_y_edit.setText(QCoreApplication.translate("vessel_form", u"0", None))
        self.move_z_label.setText(QCoreApplication.translate("vessel_form", u"Z:", None))
        self.move_z_edit.setText(QCoreApplication.translate("vessel_form", u"0", None))
        self.rotate_title_label.setText(QCoreApplication.translate("vessel_form", u"<html><head/><body><p>Rotate the basis along the axis<br/>(in degrees at following order: <span style=\" font-weight:700;\">Y, X, Z</span>)</p></body></html>", None))
        self.rotate_x_label.setText(QCoreApplication.translate("vessel_form", u"X:", None))
        self.rotate_x_edit.setText(QCoreApplication.translate("vessel_form", u"0", None))
        self.rotate_y_label.setText(QCoreApplication.translate("vessel_form", u"Y:", None))
        self.rotate_y_edit.setText(QCoreApplication.translate("vessel_form", u"0", None))
        self.rotate_z_label.setText(QCoreApplication.translate("vessel_form", u"Z:", None))
        self.rotate_z_edit.setText(QCoreApplication.translate("vessel_form", u"0", None))
        self.button_transform_accept.setText(QCoreApplication.translate("vessel_form", u"Accept", None))
        self.button_transform_clear.setText(QCoreApplication.translate("vessel_form", u"Clear", None))
        self.check_vessel.setText(QCoreApplication.translate("vessel_form", u"Check Vessel Loading", None))
        self.accept_exit.setText(QCoreApplication.translate("vessel_form", u"Accept and close window", None))
        self.label_2.setText(QCoreApplication.translate("vessel_form", u"<html><head/><body><p>All the files added to the scene have to be prepared and share a coordinate system. <br/>If '<span style=\" font-style:italic;\">None</span>' is selected as the material, the modele is considered as an Absorbing surface<span style=\" font-weight:700;\">.</span></p></body></html>", None))
    # retranslateUi

