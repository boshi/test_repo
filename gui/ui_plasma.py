# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'plasma.ui'
##
## Created by: Qt User Interface Compiler version 6.6.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QCheckBox, QHBoxLayout, QLabel,
    QLineEdit, QPushButton, QSizePolicy, QSpacerItem,
    QVBoxLayout, QWidget)

class Ui_plasma_form(object):
    def setupUi(self, plasma_form):
        if not plasma_form.objectName():
            plasma_form.setObjectName(u"plasma_form")
        plasma_form.setWindowModality(Qt.WindowModal)
        plasma_form.resize(672, 422)
        self.label = QLabel(plasma_form)
        self.label.setObjectName(u"label")
        self.label.setGeometry(QRect(10, 0, 781, 41))
        font = QFont()
        font.setPointSize(14)
        self.label.setFont(font)
        self.verticalLayoutWidget = QWidget(plasma_form)
        self.verticalLayoutWidget.setObjectName(u"verticalLayoutWidget")
        self.verticalLayoutWidget.setGeometry(QRect(10, 53, 651, 361))
        self.verticalLayout = QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.eqdsk_file_label = QLabel(self.verticalLayoutWidget)
        self.eqdsk_file_label.setObjectName(u"eqdsk_file_label")

        self.verticalLayout.addWidget(self.eqdsk_file_label)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.eqdsk_file_line = QLineEdit(self.verticalLayoutWidget)
        self.eqdsk_file_line.setObjectName(u"eqdsk_file_line")

        self.horizontalLayout.addWidget(self.eqdsk_file_line)

        self.eqdsk_file_select = QPushButton(self.verticalLayoutWidget)
        self.eqdsk_file_select.setObjectName(u"eqdsk_file_select")

        self.horizontalLayout.addWidget(self.eqdsk_file_select)

        self.eqdsk_file_clear = QPushButton(self.verticalLayoutWidget)
        self.eqdsk_file_clear.setObjectName(u"eqdsk_file_clear")

        self.horizontalLayout.addWidget(self.eqdsk_file_clear)


        self.verticalLayout.addLayout(self.horizontalLayout)

        self.species_label = QLabel(self.verticalLayoutWidget)
        self.species_label.setObjectName(u"species_label")

        self.verticalLayout.addWidget(self.species_label)

        self.horizontalLayout_6 = QHBoxLayout()
        self.horizontalLayout_6.setObjectName(u"horizontalLayout_6")
        self.species_file_line = QLineEdit(self.verticalLayoutWidget)
        self.species_file_line.setObjectName(u"species_file_line")

        self.horizontalLayout_6.addWidget(self.species_file_line)

        self.species_file_select = QPushButton(self.verticalLayoutWidget)
        self.species_file_select.setObjectName(u"species_file_select")

        self.horizontalLayout_6.addWidget(self.species_file_select)

        self.species_file_clear = QPushButton(self.verticalLayoutWidget)
        self.species_file_clear.setObjectName(u"species_file_clear")

        self.horizontalLayout_6.addWidget(self.species_file_clear)


        self.verticalLayout.addLayout(self.horizontalLayout_6)

        self.core_plasma_label = QLabel(self.verticalLayoutWidget)
        self.core_plasma_label.setObjectName(u"core_plasma_label")

        self.verticalLayout.addWidget(self.core_plasma_label)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.core_plasma_line = QLineEdit(self.verticalLayoutWidget)
        self.core_plasma_line.setObjectName(u"core_plasma_line")

        self.horizontalLayout_2.addWidget(self.core_plasma_line)

        self.core_plasma_select = QPushButton(self.verticalLayoutWidget)
        self.core_plasma_select.setObjectName(u"core_plasma_select")

        self.horizontalLayout_2.addWidget(self.core_plasma_select)

        self.core_plasma_clear = QPushButton(self.verticalLayoutWidget)
        self.core_plasma_clear.setObjectName(u"core_plasma_clear")

        self.horizontalLayout_2.addWidget(self.core_plasma_clear)


        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer)

        self.solps_enable = QCheckBox(self.verticalLayoutWidget)
        self.solps_enable.setObjectName(u"solps_enable")
        self.solps_enable.setChecked(False)

        self.verticalLayout.addWidget(self.solps_enable)

        self.edge_plasma_label = QLabel(self.verticalLayoutWidget)
        self.edge_plasma_label.setObjectName(u"edge_plasma_label")

        self.verticalLayout.addWidget(self.edge_plasma_label)

        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.edge_plasma_line = QLineEdit(self.verticalLayoutWidget)
        self.edge_plasma_line.setObjectName(u"edge_plasma_line")
        self.edge_plasma_line.setEnabled(False)
        self.edge_plasma_line.setReadOnly(False)

        self.horizontalLayout_3.addWidget(self.edge_plasma_line)

        self.edge_plasma_select = QPushButton(self.verticalLayoutWidget)
        self.edge_plasma_select.setObjectName(u"edge_plasma_select")
        self.edge_plasma_select.setEnabled(False)

        self.horizontalLayout_3.addWidget(self.edge_plasma_select)

        self.edge_plasma_clear = QPushButton(self.verticalLayoutWidget)
        self.edge_plasma_clear.setObjectName(u"edge_plasma_clear")

        self.horizontalLayout_3.addWidget(self.edge_plasma_clear)


        self.verticalLayout.addLayout(self.horizontalLayout_3)

        self.edge_mesh_label = QLabel(self.verticalLayoutWidget)
        self.edge_mesh_label.setObjectName(u"edge_mesh_label")

        self.verticalLayout.addWidget(self.edge_mesh_label)

        self.horizontalLayout_4 = QHBoxLayout()
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.edge_mesh_line = QLineEdit(self.verticalLayoutWidget)
        self.edge_mesh_line.setObjectName(u"edge_mesh_line")
        self.edge_mesh_line.setEnabled(False)
        self.edge_mesh_line.setReadOnly(False)

        self.horizontalLayout_4.addWidget(self.edge_mesh_line)

        self.edge_mesh_select = QPushButton(self.verticalLayoutWidget)
        self.edge_mesh_select.setObjectName(u"edge_mesh_select")
        self.edge_mesh_select.setEnabled(False)

        self.horizontalLayout_4.addWidget(self.edge_mesh_select)

        self.edge_mesh_clear = QPushButton(self.verticalLayoutWidget)
        self.edge_mesh_clear.setObjectName(u"edge_mesh_clear")

        self.horizontalLayout_4.addWidget(self.edge_mesh_clear)


        self.verticalLayout.addLayout(self.horizontalLayout_4)

        self.verticalSpacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer_2)

        self.horizontalLayout_5 = QHBoxLayout()
        self.horizontalLayout_5.setObjectName(u"horizontalLayout_5")
        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_5.addItem(self.horizontalSpacer)

        self.eqdsk_verbose = QCheckBox(self.verticalLayoutWidget)
        self.eqdsk_verbose.setObjectName(u"eqdsk_verbose")

        self.horizontalLayout_5.addWidget(self.eqdsk_verbose)

        self.impurity_verbose = QCheckBox(self.verticalLayoutWidget)
        self.impurity_verbose.setObjectName(u"impurity_verbose")

        self.horizontalLayout_5.addWidget(self.impurity_verbose)

        self.plasma_check = QPushButton(self.verticalLayoutWidget)
        self.plasma_check.setObjectName(u"plasma_check")

        self.horizontalLayout_5.addWidget(self.plasma_check)

        self.accept_exit = QPushButton(self.verticalLayoutWidget)
        self.accept_exit.setObjectName(u"accept_exit")

        self.horizontalLayout_5.addWidget(self.accept_exit)


        self.verticalLayout.addLayout(self.horizontalLayout_5)


        self.retranslateUi(plasma_form)

        QMetaObject.connectSlotsByName(plasma_form)
    # setupUi

    def retranslateUi(self, plasma_form):
        plasma_form.setWindowTitle(QCoreApplication.translate("plasma_form", u"Plasma setup module", None))
        self.label.setText(QCoreApplication.translate("plasma_form", u"Define files with plasma simulation results", None))
        self.eqdsk_file_label.setText(QCoreApplication.translate("plasma_form", u"EQDSK file", None))
        self.eqdsk_file_select.setText(QCoreApplication.translate("plasma_form", u"Select", None))
        self.eqdsk_file_clear.setText(QCoreApplication.translate("plasma_form", u"Clear", None))
        self.species_label.setText(QCoreApplication.translate("plasma_form", u"Plasma species data file", None))
        self.species_file_select.setText(QCoreApplication.translate("plasma_form", u"Select", None))
        self.species_file_clear.setText(QCoreApplication.translate("plasma_form", u"Clear", None))
        self.core_plasma_label.setText(QCoreApplication.translate("plasma_form", u"Core plasma data file", None))
        self.core_plasma_select.setText(QCoreApplication.translate("plasma_form", u"Select", None))
        self.core_plasma_clear.setText(QCoreApplication.translate("plasma_form", u"Clear", None))
        self.solps_enable.setText(QCoreApplication.translate("plasma_form", u"Blend with edge plasma from SOLPS", None))
        self.edge_plasma_label.setText(QCoreApplication.translate("plasma_form", u"Edge plasma data file:", None))
        self.edge_plasma_select.setText(QCoreApplication.translate("plasma_form", u"Select", None))
        self.edge_plasma_clear.setText(QCoreApplication.translate("plasma_form", u"Clear", None))
        self.edge_mesh_label.setText(QCoreApplication.translate("plasma_form", u"Edge mesh file:", None))
        self.edge_mesh_select.setText(QCoreApplication.translate("plasma_form", u"Select", None))
        self.edge_mesh_clear.setText(QCoreApplication.translate("plasma_form", u"Clear", None))
        self.eqdsk_verbose.setText(QCoreApplication.translate("plasma_form", u"Plot Equilibrium", None))
        self.impurity_verbose.setText(QCoreApplication.translate("plasma_form", u"Plot main impurity", None))
        self.plasma_check.setText(QCoreApplication.translate("plasma_form", u"Check Plasma Creation", None))
        self.accept_exit.setText(QCoreApplication.translate("plasma_form", u"Accept and close this window", None))
    # retranslateUi

