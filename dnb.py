# This Python file uses the following encoding: utf-8
from PySide6.QtWidgets import QDialog

from gui.ui_dnb_form import Ui_dnb_form

from modules.classes import beam_settings
from modules.gui_modules import show_warning
from modules.common import neutralization_efficiensy

from modules.syscheck import system_check



class dnb(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = Ui_dnb_form()
        self.ui.setupUi(self)

        self.setFixedSize(300, 600)

        self.ui.check_beam_creation.clicked.connect(self.check_beam_creation)

    ## Check beam creation -----------------------------------------------------
    @system_check
    def check_beam_creation(self, beam_parameters = None, verbose = True):
        from cherab_modules.beam_setter import beam_list_setter

        try:
            if beam_parameters is None:
                beam_parameters = self.retreve_settings()

            beam_list_setter(beam_parameters)

            if verbose:
                show_warning('Beam creation is ok')
            else:
                return True
        except:
            if verbose:
                show_warning('Something is wrong')
            else:
                return False


    # Common function
    def retreve_settings(self):
        settings_ok = True
        composition = [1., 0., 0.]

        ## Getting data from drop-down lists
        element_name = self.ui.beam_element.currentText()
        if element_name == 'hydrogen':
            element = 'h'
        else:
            element = 'd'
        atom_power_type = True if self.ui.beam_power_type.currentText() == 'Atoms' else False
        beam_components = 1 if self.ui.beam_components.currentText() == '1' else 3

        ## Getting data from line edits
        energy      = self.ui.beam_energy.value()
        radius      = self.ui.beam_radius.value()
        divergence  = self.ui.beam_divergence.value()
        temperature = self.ui.beam_temperature.value()
        power       = self.ui.beam_power.value()
        length      = self.ui.beam_length.value()

        ## Getting beam composition
        if beam_components == 3:
            composition_0 = self.ui.fraction_e0.value()
            composition_2 = self.ui.fraction_e2.value()
            composition_3 = self.ui.fraction_e3.value()

            if not any(point is None for point in [composition_0, composition_2, composition_3]):
                composition = [composition_0, composition_2, composition_3]
            else:
                composition = [1., 0., 0.]

            if sum(composition) - 1 > 0.001:
                show_warning(f'Fractions must add up to 1, now it is {sum(composition)}')
                settings_ok = False

        ## Getting beam power in atoms
        if atom_power_type:
            atom_power = [fraction * power for fraction in composition]
        else:
            ion_power = power
            ion_curent = ion_power / energy
            atom_current = []
            atom_power= []
            for i in range(3):
                neutralisation_eff = neutralization_efficiensy(energy/(i+1))
                atom_current.append(ion_curent * neutralisation_eff * composition[i] * (i + 1))
                atom_power.append(atom_current[-1] * energy/(i+1))

        ## Getting beam start point
        start_x = self.ui.beam_start_x.value()
        start_y = self.ui.beam_start_y.value()
        start_z = self.ui.beam_start_z.value()
        start = [start_x, start_y, start_z]

        ## Getting beam focus point
        focus_x = self.ui.beam_focus_x.value()
        focus_y = self.ui.beam_focus_y.value()
        focus_z = self.ui.beam_focus_z.value()
        focus = [focus_x, focus_y, focus_z]

        if settings_ok:
            return beam_settings(element, energy, radius, atom_power, divergence, temperature, start, focus, length)
        else:
            return None


    # --------------------------------------------------------------------------

    def closeEvent(self, event):
        self.reject()
        self.destroy()