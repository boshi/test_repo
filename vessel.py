# This Python file uses the following encoding: utf-8
import os
from PySide6 import QtCore
from PySide6.QtWidgets import QWidget, QFileDialog, QSizePolicy, QSpacerItem, QMessageBox

from gui.ui_vessel import Ui_vessel_form
from gui.vessel_cad_form import vessel_widget

from modules.classes import stored_model, transform_setting
from modules.syscheck import system_check

class vessel(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = Ui_vessel_form()
        self.ui.setupUi(self)
        self.vessel_count = 0
        self.vessels = []
        self.cad_models = []
        self.base_dir = None
        self.transform_setting = transform_setting()
        self.startup_prep()

        self.ui.add_model.clicked.connect(self.add)
        self.ui.remove_model.clicked.connect(self.delete_widget)
        self.ui.check_vessel.clicked.connect(self.check_loading_pushed)
        self.ui.button_transform_accept.clicked.connect(self.transform_accept_pushed)
        self.ui.button_transform_clear.clicked.connect(self.transform_clear_pushed)


    # Add model button pushed --------------------------------------------------
    @QtCore.Slot()
    def add(self):
        self.vessel_count += 1
        file_name, _ = QFileDialog.getOpenFileName(self, f'Select CAD model file', self.base_dir)
        self.base_dir = os.path.dirname(file_name)

        if not file_name == '':
            self.vessels.append(vessel_widget(self, self.vessel_count, file_name))

            self.ui.verticalLayout_1.removeItem(self.ui.verticalSpacer_2)

            self.ui.verticalLayout_1.addWidget(self.vessels[-1])

            self.ui.verticalLayout_1.addItem(self.ui.verticalSpacer_2)
    # --------------------------------------------------------------------------


    # Delete last model button pushed ------------------------------------------
    @QtCore.Slot()
    def delete_widget(self):
        if self.vessel_count > 0:
            self.vessel_count -= 1
            widget_to_delete = self.vessels[-1]
            parent_widget = widget_to_delete.parent()
            parent_widget.layout().removeWidget(widget_to_delete)

            widget_to_delete.deleteLater()

            self.vessels.pop(-1)
    # --------------------------------------------------------------------------


    # Check model loading button pushed ----------------------------------------
    @QtCore.Slot()
    def check_loading_pushed(self):
        self.check_loading()

    # Common function
    @system_check
    def check_loading(self, verbose = True):
        from cherab_modules import load_cad_models

        model_list = self.retreve_settings()
        loaded_models = load_cad_models(model_list, self.transform)

        if type(loaded_models) is list:
            if verbose:
                dlg = QMessageBox()
                dlg.setText("All models loaded without errors")
                dlg.exec()
            else:
                return True
        else:
            if verbose:
                dlg = QMessageBox()
                dlg.setText(f"There was a problem with model {loaded_models}")
                dlg.exec()
            else:
                return loaded_models

    # Common function
    def retreve_settings(self, index = None):
        if index is None:
            model_file_list = []
            model_list = self.vessels
            for model in model_list:
                model_file_list.append(stored_model(model.path_line.text(), model.material_dropdown.currentText(), model.roughness_spin.value()))

            return model_file_list

        else:
            if index is int:
                model = self.vessels[index]
                return stored_model(model.path_line.text(), model.material_dropdown.currentText(), model.roughness_spin.value())
            else:
                print("Index must bi an integer")
    # --------------------------------------------------------------------------



    # Transform accept button pushed -------------------------------------------
    def transform_accept_pushed(self):
        shift_list = self.transform_setting.transform_list
        rotate_list = self.transform_setting.rotate_list

        shift_list[0] =   self.catch_and_return(self.ui.move_x_edit, shift_list[0])
        shift_list[1] =   self.catch_and_return(self.ui.move_y_edit, shift_list[1])
        shift_list[2] =   self.catch_and_return(self.ui.move_z_edit, shift_list[2])
        rotate_list[0] =  self.catch_and_return(self.ui.rotate_x_edit, rotate_list[0])
        rotate_list[1] =  self.catch_and_return(self.ui.rotate_y_edit, rotate_list[1])
        rotate_list[2] =  self.catch_and_return(self.ui.rotate_z_edit, rotate_list[2])

        if None in shift_list + rotate_list:
            dlg = QMessageBox()
            dlg.setText("Only float numbers are allowed")
            dlg.exec()
        else:
            self.transform_setting = transform_setting(shift_list, rotate_list)
            self.update_transform()

    # Common function
    def update_transform(self):
        from raysect.optical import translate, rotate

        shift_list = self.transform_setting.transform_list
        rotate_list = self.transform_setting.rotate_list
        self.transform = translate(shift_list[0], shift_list[1], shift_list[2]) * rotate(-rotate_list[1], -rotate_list[0], rotate_list[2])

    # Common function
    def catch_and_return(self, edit, fallback):
        try:
            value = float(edit.text())
        except ValueError:
            edit.setText(f'{fallback}')
            value = None

        return value
    # --------------------------------------------------------------------------


    # Transform clear button pushed --------------------------------------------
    def transform_clear_pushed(self):
        shift_list  = [0, 0, 0]
        rotate_list = [0, 0, 0]

        self.ui.move_x_edit.setText(f'{shift_list[0]}')
        self.ui.move_y_edit.setText(f'{shift_list[1]}')
        self.ui.move_z_edit.setText(f'{shift_list[2]}')
        self.ui.rotate_x_edit.setText(f'{rotate_list[0]}')
        self.ui.rotate_y_edit.setText(f'{rotate_list[1]}')
        self.ui.rotate_z_edit.setText(f'{rotate_list[2]}')

        self.transform_setting = transform_setting(shift_list, rotate_list)
        self.update_transform()
    # --------------------------------------------------------------------------


    @system_check
    def startup_prep(self):
        self.update_transform()
