# This Python file uses the following encoding: utf-8
import os

from PySide6 import QtCore
from PySide6.QtWidgets import QWidget, QFileDialog, QMessageBox
from PySide6 import QtQuick

from gui.ui_plasma import Ui_plasma_form

from modules.syscheck import system_check
from modules.classes import plasma_files

class plasma(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = Ui_plasma_form()
        self.ui.setupUi(self)
        self.base_dir = None
        self.ui.solps_enable.setChecked(False)
        self.solps_check_state()

        self.ui.eqdsk_file_select.clicked.connect(self.eqdsk_select_pushed)
        self.ui.eqdsk_file_clear.clicked.connect(self.eqdsk_clear_pushed)

        self.ui.species_file_select.clicked.connect(self.species_select_pushed)
        self.ui.species_file_clear.clicked.connect(self.species_clear_pushed)

        self.ui.core_plasma_select.clicked.connect(self.core_plasma_select_pushed)
        self.ui.core_plasma_clear.clicked.connect(self.core_plasma_clear_pushed)

        self.ui.edge_plasma_select.clicked.connect(self.edge_plasma_select_pushed)
        self.ui.edge_plasma_clear.clicked.connect(self.edge_plasma_clear_pushed)

        self.ui.edge_mesh_select.clicked.connect(self.edge_mesh_select_pushed)
        self.ui.edge_mesh_clear.clicked.connect(self.edge_mesh_clear_pushed)

        self.ui.solps_enable.clicked.connect(self.solps_checked)
        self.ui.plasma_check.clicked.connect(self.plasma_check_pushed)


    @QtCore.Slot()
    def eqdsk_select_pushed(self):
        self.select_pushed(self.ui.eqdsk_file_line, 'EQDSK')

    @QtCore.Slot()
    def core_plasma_select_pushed(self):
        self.select_pushed(self.ui.core_plasma_line, 'Core Plasma')

    @QtCore.Slot()
    def edge_plasma_select_pushed(self):
        self.select_pushed(self.ui.edge_plasma_line, 'Edge Plasma')

    @QtCore.Slot()
    def edge_mesh_select_pushed(self):
        self.select_pushed(self.ui.edge_mesh_line, 'Edge Mesh')

    @QtCore.Slot()
    def species_select_pushed(self):
        self.select_pushed(self.ui.species_file_line, 'Plasma Species')



    @QtCore.Slot()
    def eqdsk_clear_pushed(self):
        self.clear_pushed(self.ui.eqdsk_file_line)

    @QtCore.Slot()
    def core_plasma_clear_pushed(self):
        self.clear_pushed(self.ui.core_plasma_line)

    @QtCore.Slot()
    def edge_plasma_clear_pushed(self):
        self.clear_pushed(self.ui.edge_plasma_line)

    @QtCore.Slot()
    def edge_mesh_clear_pushed(self):
        self.clear_pushed(self.ui.edge_mesh_line)

    @QtCore.Slot()
    def species_clear_pushed(self):
        self.clear_pushed(self.ui.species_file_line)
    
    @QtCore.Slot()
    def solps_checked(self):
        self.solps_check_state()

    def select_pushed(self, line, file_type):
        file_name, _ = QFileDialog.getOpenFileName(self, f'Select {file_type} file', self.base_dir)
        self.base_dir = os.path.dirname(file_name)
        line.setText(file_name)

    def clear_pushed(self, line):
        line.setText('')

    def solps_check_state(self):
        if self.ui.solps_enable.isChecked():
            self.ui.edge_plasma_line.setEnabled(True)
            self.ui.edge_mesh_line.setEnabled(True)
            self.ui.edge_mesh_select.setEnabled(True)
            self.ui.edge_plasma_select.setEnabled(True)
        else:
            self.ui.edge_plasma_line.setEnabled(False)
            self.ui.edge_mesh_line.setEnabled(False)
            self.ui.edge_mesh_select.setEnabled(False)
            self.ui.edge_plasma_select.setEnabled(False)
            self.clear_pushed(self.ui.edge_mesh_line)
            self.clear_pushed(self.ui.edge_plasma_line)

    @QtCore.Slot()
    def plasma_check_pushed(self):
        self.plasma_check()

    @system_check
    def plasma_check(self):
        files = plasma_files()

        files.eqdsk.set(self.ui.eqdsk_file_line.text())
        files.plasma_species.set(self.ui.species_file_line.text())
        files.plasma_core.set(self.ui.core_plasma_line.text())

        if self.ui.solps_enable.isChecked():
            files.plasma_edge.set(self.ui.edge_plasma_line.text())
            files.mesh.set(self.ui.edge_mesh_line.text())

        if files.check_all() == 0:
            from cherab_modules import create_plasma
            from cherab_modules.plots import plot_created_plasma

            eqdsk_verbose    = self.ui.eqdsk_verbose.isChecked()
            impurity_verbose = self.ui.impurity_verbose.isChecked()

            equilibrium, plasma = create_plasma(files)
            plot_created_plasma(plasma, equilibrium, impurity_verbose, eqdsk_verbose)
        
        else:
            dlg = QMessageBox()
            dlg.setText("Not all paths to required files are set")
            dlg.exec()
    
