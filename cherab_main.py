import sys, pickle

from copy import deepcopy as cp

from PySide6.QtWidgets import QApplication, QMainWindow, QMessageBox, QFileDialog
from PySide6 import QtCore
from PySide6.QtCore import QUrl
from PySide6.QtGui import QDesktopServices

from gui.ui_form import Ui_Cherab_main
from plasma import plasma
from vessel import vessel
from dnb import dnb
from nbi import nbi
from emission import emission

from modules.classes import cherab_settings, plasma_files
from modules.species_parser import *
from modules.syscheck import system_check
from modules.gui_modules import show_warning

"""
    # TODO: clear button for emission
"""


class Cherab_main(QMainWindow):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = Ui_Cherab_main()
        self.ui.setupUi(self)
        self.scene = []
        self.base_dir = None

        self.plasma_ui  = plasma()
        self.vessel_ui  = vessel()
        self.dnb_ui     = dnb()
        self.nbi_ui     = nbi()
        self.emission = emission()

        self.settings = cherab_settings(self.ui.check_plasma.isChecked(), self.ui.check_vessel.isChecked(), self.ui.check_dnb.isChecked(), self.ui.check_nbi.isChecked())


        self.ui.open_plasma.clicked.connect(self.open_plasma_ui)
        self.ui.open_vessel.clicked.connect(self.open_vessel_ui)
        self.ui.open_dnb.clicked.connect(self.open_dnb_ui)
        self.ui.open_nbi.clicked.connect(self.open_nbi_ui)
        self.ui.open_emission.clicked.connect(self.open_emission_ui)

        self.ui.skip_vessel.clicked.connect(self.skip_vessel_pushed)
        self.ui.skip_dnb.clicked.connect(self.skip_dnb_pushed)
        self.ui.skip_nbi.clicked.connect(self.skip_nbi_pushed)
        self.ui.skip_plasma.clicked.connect(self.skip_plasma_pushed)

        self.plasma_ui.ui.accept_exit.clicked.connect(self.plasma_accept_pushed)
        self.vessel_ui.ui.accept_exit.clicked.connect(self.vessel_accept_pushed)
        self.dnb_ui.ui.apply_and_close.clicked.connect(self.dnb_accept_pushed)
        self.nbi_ui.ui.accept_close_nbi.clicked.connect(self.nbi_accept_pushed)

        self.ui.actionCherab_Documentation.triggered.connect(self.cherab_documentation_pushed)
        self.ui.actionRaysect_Documentation.triggered.connect(self.raysect_documentation_pushed)
        self.ui.actionAbout.triggered.connect(self.about_pushed)
        self.ui.actionSave_current_settings.triggered.connect(self.save_settings_pushed)
        self.ui.actionLoad_saved_settings.triggered.connect(self.load_settings_pushed)



    ## Buttons that open setup windows for every module ------------------------
    @QtCore.Slot()
    def open_plasma_ui(self):
        self.plasma_ui.show()

    @QtCore.Slot()
    def open_vessel_ui(self):
        self.vessel_ui.show()

    @QtCore.Slot()
    def open_dnb_ui(self):
        self.dnb_ui.show()

    @QtCore.Slot()
    def open_nbi_ui(self):
        self.nbi_ui.show()

    @QtCore.Slot()
    def open_emission_ui(self):

        if self.settings.setup_status.ready_for_emission():
            settings = cp(self.settings)
            self.emission.get_settings(settings)
            self.emission.update_species()
            self.emission.apply_settings()
            self.emission.show()
        else:
            show_warning('Not all modules are setted up or skipped')
    # --------------------------------------------------------------------------


    ## Skiping checkboxes-------------------------------------------------------
    @QtCore.Slot()
    def skip_plasma_pushed(self):
        status = self.skip_pushed(self.ui.skip_plasma, self.ui.check_plasma)
        self.settings.setup_status.update_plasma(status)

    @QtCore.Slot()
    def skip_vessel_pushed(self):
        status = self.skip_pushed(self.ui.skip_vessel, self.ui.check_vessel)
        self.settings.setup_status.update_vessel(status)
        print(self.settings.setup_status._vessel)

    @QtCore.Slot()
    def skip_dnb_pushed(self):
        status = self.skip_pushed(self.ui.skip_dnb, self.ui.check_dnb)
        self.settings.setup_status.update_dnb(status)
        print(self.settings.setup_status._dnb)

    @QtCore.Slot()
    def skip_nbi_pushed(self):
        status = self.skip_pushed(self.ui.skip_nbi, self.ui.check_nbi)
        self.settings.setup_status.update_nbi(status)
        print(self.settings.setup_status._nbi)

    # Common function
    def skip_pushed(self, button, checkbox):
        current_text = button.text()
        if not current_text == 'Clear':
            checkbox.toggle()
            if checkbox.isChecked():
                new_text = 'Unskip'
            else:
                new_text = 'Skip'

            button.setText(new_text)
        else:
            self.clear_stored_settings(button)
            if button.objectName() == 'skip_plasma':
                button.setEnabled(False)
            else:
                button.setText('Skip')

            checkbox.setChecked(False)

        if checkbox.isChecked():
            return None
        else:
            return False

    # Common function
    def clear_stored_settings(self, button):
        if button.objectName() == 'skip_plasma':
            self.plasma_ui.deleteLater()
            self.plasma_ui = plasma()
            self.ui.open_plasma.setEnabled(True)
            self.plasma_ui.ui.accept_exit.clicked.connect(self.plasma_accept_pushed)
            self.settings.plasma_files.clear()

        elif button.objectName() == 'skip_vessel':
            self.vessel_ui.deleteLater()
            self.vessel_ui = vessel()
            self.ui.open_vessel.setEnabled(True)
            self.vessel_ui.ui.accept_exit.clicked.connect(self.vessel_accept_pushed)
            self.settings.cad_models = []

        elif button.objectName() == 'skip_dnb':
            self.dnb_ui.deleteLater()
            self.dnb_ui = dnb()
            self.ui.open_dnb.setEnabled(True)
            self.dnb_ui.ui.apply_and_close.clicked.connect(self.dnb_accept_pushed)
            self.settings.dnb_settings.clear()

        elif button.objectName() == 'skip_nbi':
            self.nbi_ui.deleteLater()
            self.nbi_ui = nbi()
            self.ui.open_nbi.setEnabled(True)
            self.nbi_ui.ui.accept_close_nbi.clicked.connect(self.nbi_accept_pushed)
            self.settings.nbi_settings = []

        #
    # --------------------------------------------------------------------------


    ## Accept and close button (Plasma) ----------------------------------------
    @QtCore.Slot()
    def plasma_accept_pushed(self):
        self.store_plasma_file_paths(self.settings.plasma_files)

        self.plasma_ui.close()

        if self.settings.plasma_files.check_all() == 0:
            self.ui.check_plasma.setChecked(True)
            self.ui.skip_plasma.setEnabled(True)
        else:
            self.ui.check_plasma.setChecked(False)

        self.settings.setup_status.update_plasma(self.ui.check_plasma.isChecked())

    # Common function
    def store_plasma_file_paths(self, settings = None):
        if settings is None:
            settings = plasma_files()

        settings.eqdsk.set(self.plasma_ui.ui.eqdsk_file_line.text())
        settings.plasma_species.set(self.plasma_ui.ui.species_file_line.text())
        settings.plasma_core.set(self.plasma_ui.ui.core_plasma_line.text())

        if self.plasma_ui.ui.solps_enable.isChecked():
            settings.plasma_edge.set(self.plasma_ui.ui.edge_plasma_line.text())
            settings.mesh.set(self.plasma_ui.ui.edge_mesh_line.text())

        return settings
    # --------------------------------------------------------------------------


    ## Accept and close button (Vessel) ----------------------------------------
    @QtCore.Slot()
    def vessel_accept_pushed(self):
        self.settings.cad_models = self.vessel_ui.retreve_settings()

        self.settings.transform_settings = self.vessel_ui.transform_setting

        check = all(model.exist for model in self.settings.cad_models)
        if not check:
            self.settings.cad_models = []
            self.ui.check_vessel.setChecked(False)

            dlg = QMessageBox()
            dlg.setText("Some selected files do not exist")
            dlg.exec()
        else:
            self.vessel_ui.close()
            self.ui.check_vessel.setChecked(True)
            self.ui.skip_vessel.setText('Clear')

        self.settings.setup_status.update_vessel(self.ui.check_vessel.isChecked())
    # --------------------------------------------------------------------------


    ## Accept and close button (DNB) -------------------------------------------
    def dnb_accept_pushed(self):
        self.dnb_ui.accept()
        self.settings.dnb_settings = self.dnb_ui.retreve_settings()
        self.dnb_ui.close()
        self.ui.check_dnb.setChecked(True)

        self.settings.setup_status.update_dnb(self.ui.check_dnb.isChecked())
        self.ui.skip_dnb.setText('Clear')
    # --------------------------------------------------------------------------

    ## Accept and close button (NBI) -------------------------------------------
    def nbi_accept_pushed(self):
        self.nbi_ui.close()

        if len(self.nbi_ui.nbi_list) > 0:
            self.settings.nbi_settings = self.nbi_ui.nbi_list
            self.ui.check_nbi.setChecked(True)

            self.settings.setup_status.update_nbi(self.ui.check_nbi.isChecked())
            self.ui.skip_nbi.setText('Clear')
    # --------------------------------------------------------------------------


    ## Menu buttons functions  -------------------------------------------------
    def cherab_documentation_pushed(self):
        url = QUrl("https://www.cherab.info/")
        QDesktopServices.openUrl(url)


    def raysect_documentation_pushed(self):
        url = QUrl("https://www.raysect.org/")
        QDesktopServices.openUrl(url)

    def about_pushed(self):
        show_warning('This is an About pop-up placeholder. Thank you very much for clicking. 10/10 click again.')

    def save_settings_pushed(self):
        dialog_data = QFileDialog.getSaveFileName(self, f'Save settings', self.base_dir, '*.chs')
        save_file_path = dialog_data[0]

        if save_file_path:
            with open(save_file_path, 'wb') as output:
                pickle.dump(self.settings, output)

    def load_settings_pushed(self):
        dialog_data = QFileDialog.getOpenFileName(self, f'Load settings', self.base_dir, '*.chs')
        open_file_path = dialog_data[0]
        if open_file_path:
            with open(open_file_path, 'rb') as input:
                self.settings = pickle.load(input)

            readyness = self.settings.setup_status.verbose()

            if readyness[0]:                                                    # Plasma settings are loaded, locking
                self.ui.skip_plasma.setEnabled(True)
                self.ui.skip_plasma.setText('Clear')
                self.ui.open_plasma.setEnabled(False)
                self.ui.check_plasma.setChecked(True)
            if readyness[1]:                                                    # Vessel settings are loaded, locking
                self.ui.skip_vessel.setEnabled(True)
                self.ui.skip_vessel.setText('Clear')
                self.ui.open_vessel.setEnabled(False)
                self.ui.check_vessel.setChecked(True)
            if readyness[2]:                                                    # DNB settings are loaded, locking
                self.ui.skip_dnb.setEnabled(True)
                self.ui.skip_dnb.setText('Clear')
                self.ui.open_dnb.setEnabled(False)
                self.ui.check_dnb.setChecked(True)
            if readyness[3]:                                                    # NBI settings are loaded, locking
                self.ui.skip_nbi.setEnabled(True)
                self.ui.skip_nbi.setText('Clear')
                self.ui.open_nbi.setEnabled(False)
                self.ui.check_nbi.setChecked(True)


    @system_check
    def startup_prep(self):
        from raysect.optical import World

        widget.scene.append(World())

    def closeEvent(self, event):
        self.plasma_ui.close()
        self.vessel_ui.close()
        self.dnb_ui.close()
        self.nbi_ui.close()
        self.emission.close()

        self.plasma_ui.deleteLater()
        self.vessel_ui.deleteLater()
        self.dnb_ui.deleteLater()
        self.nbi_ui.deleteLater()
        self.emission.deleteLater()

#===============================================================================
#===============================================================================

if __name__ == "__main__":
    app = QApplication(sys.argv)
    widget = Cherab_main()
    widget.startup_prep()
    widget.show()

    sys.exit(app.exec())
