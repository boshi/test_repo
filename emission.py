# This Python file uses the following encoding: utf-8
import os
from PySide6 import QtCore
from PySide6.QtWidgets import QWidget, QFileDialog, QFrame, QLineEdit, QSizePolicy, QSpacerItem, QMessageBox, QLabel, QGridLayout, QComboBox, QSpinBox
from PySide6.QtCore import Qt

from gui.ui_emission_form import Ui_emission_form

from modules.syscheck import system_check
from modules.gui_modules import show_warning, inline_widget
from modules.species_parser import *
from modules.classes import line_settings, cherab_settings, fibre_optic, line_of_sight, pinhole_camera

from cherab_modules.impurity_fractions import get_element
from cherab_modules.load_from_settings import create_plasma_lines, create_beam_lines

class emission(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = Ui_emission_form()
        self.ui.setupUi(self)

        self.settings = cherab_settings()

        self.plasma_species_list = []
        self.nbi_element = None
        self.dnb_element = None

        self.plasma_line_counter = 0
        self.dnb_line_counter    = 0
        self.nbi_line_counter    = 0

        self.ui.scrollArea_plasma.setWidgetResizable(True)
        self.ui.scrollArea_dnb.setWidgetResizable(True)
        self.ui.scrollArea_nbi.setWidgetResizable(True)

        self.spaser_plasma = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.spaser_dnb    = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.spaser_nbi    = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.plasma_lines_widget = QWidget()
        self.ui.scrollArea_plasma.setWidget(self.plasma_lines_widget)
        self.plasma_lines_layout = QGridLayout(self.plasma_lines_widget)
        self.plasma_lines_layout.addItem(self.spaser_plasma)

        self.dnb_lines_widget = QWidget()
        self.ui.scrollArea_dnb.setWidget(self.dnb_lines_widget)
        self.dnb_lines_layout = QGridLayout(self.dnb_lines_widget)
        self.dnb_lines_layout.addItem(self.spaser_dnb)

        self.nbi_lines_widget = QWidget()
        self.ui.scrollArea_nbi.setWidget(self.nbi_lines_widget)
        self.nbi_lines_layout = QGridLayout(self.nbi_lines_widget)
        self.nbi_lines_layout.addItem(self.spaser_nbi)

        self.ui.button_add_plasma_line.clicked.connect(self.add_plasma_line)
        self.ui.button_add_nbi_line.clicked.connect(self.add_nbi_line)
        self.ui.button_add_dnb_line.clicked.connect(self.add_dnb_line)
        self.ui.button_remove_plasma_line.clicked.connect(self.remove_last_plasma_line)
        self.ui.button_remove_nbi_line.clicked.connect(self.remove_last_nbi_line)
        self.ui.button_remove_dnb_line.clicked.connect(self.remove_last_dnb_line)
        self.ui.button_accept_emission.clicked.connect(self.start_sumulation)
        self.ui.check_lines_button.clicked.connect(self.check_lines_pushed)
        self.ui.check_observer_button.clicked.connect(self.check_observer_pushed)
        self.ui.check_reflections.toggled.connect(self.reflection_check_toggled)
        self.ui.comboBox_observer_select.currentIndexChanged.connect(self.observer_selection_toggled)

    def update_species(self):
        gas, impurities, _ = species_parser(self.settings.plasma_files.plasma_species.path)
        species = [gas] + impurities

        if self.settings.setup_status._dnb:
            dnb_element = self.settings.dnb_settings.element
        else:
            dnb_element = None

        if self.settings.setup_status._nbi:
            nbi_element = self.settings.nbi_settings[0].element
        else:
            nbi_element = None


        self.plasma_species_list = species
        self.nbi_element = nbi_element
        self.dnb_element = dnb_element

    def apply_settings(self):
        if self.settings.setup_status._dnb is None:
            self.ui.button_add_dnb_line.setEnabled(False)
        else:
            self.ui.button_add_dnb_line.setEnabled(True)

        if self.settings.setup_status._nbi is None:
            self.ui.button_add_nbi_line.setEnabled(False)
        else:
            self.ui.button_add_nbi_line.setEnabled(True)

        if self.settings.setup_status._vessel is None:
            self.ui.check_reflections.setEnabled(False)
        else:
            self.ui.check_reflections.setEnabled(True)


    def get_settings(self, settings):
        self.settings = settings

        # self.settings.verbose()

    def add_plasma_line(self):
        self.plasma_line_counter += 1
        self.plasma_lines_layout.removeItem(self.spaser_plasma)
        self.add_line(self.plasma_lines_layout, self.plasma_line_counter)
        self.plasma_lines_layout.addItem(self.spaser_plasma)

    def add_line(self, layout, number):
        number_title = QLabel(f'{number}')
        element      = QComboBox()
        element.addItems(self.plasma_species_list)
        charge       = QSpinBox()
        S1           = QSpinBox()
        S1.setValue(1)
        arrow        = QLabel('→')
        S2           = QSpinBox()
        type         = QComboBox()
        type.addItems(['Exitation', 'Recombination'])
        shape        = QComboBox()
        shape.addItems(['Gaussian', 'Multiplet', 'Stark Broadened', 'Zeeman line shape', 'Zeeman Triplet', 'Zeeman Multiplet', 'Parametrised Triplet'])

        layout.addWidget(number_title, number-1, 0, Qt.AlignLeft)
        layout.addWidget(element     , number-1, 1, Qt.AlignLeft)
        layout.addWidget(charge      , number-1, 2, Qt.AlignCenter)

        layout.addWidget(S1          , number-1, 3, Qt.AlignRight)
        layout.addWidget(arrow       , number-1, 4, Qt.AlignCenter)
        layout.addWidget(S2          , number-1, 5, Qt.AlignLeft)

        layout.addWidget(type        , number-1, 6, Qt.AlignCenter)
        layout.addWidget(shape       , number-1, 7, Qt.AlignCenter)

    def add_nbi_line(self):
        self.nbi_line_counter += 1
        self.nbi_lines_layout.removeItem(self.spaser_nbi)
        self.add_beam_line(self.nbi_lines_layout, self.nbi_line_counter)
        self.nbi_lines_layout.addItem(self.spaser_nbi)

    def add_dnb_line(self):
        self.dnb_line_counter += 1
        self.dnb_lines_layout.removeItem(self.spaser_dnb)
        self.add_beam_line(self.dnb_lines_layout, self.dnb_line_counter)
        self.dnb_lines_layout.addItem(self.spaser_dnb)


    def add_beam_line(self, layout, number):
        number_title = QLabel(f'{number}')
        type         = QComboBox()
        type.addItems(['Exitation', 'CX'])
        S1           = QSpinBox()
        S1.setValue(1)
        arrow        = QLabel('→')
        S2           = QSpinBox()
        shape        = QLineEdit('None')
        shape.setMinimumWidth(280)

        layout.addWidget(number_title, number-1, 0, Qt.AlignLeft)
        layout.addWidget(type        , number-1, 1, Qt.AlignLeft)

        layout.addWidget(S1          , number-1, 2, Qt.AlignRight)
        layout.addWidget(arrow       , number-1, 3, Qt.AlignCenter)
        layout.addWidget(S2          , number-1, 4, Qt.AlignLeft)

        layout.addWidget(shape       , number-1, 5, Qt.AlignRight)


    def remove_last_plasma_line(self):
        if self.plasma_line_counter > 0:
            self.remove_last_line(self.plasma_line_counter, self.plasma_lines_layout)
            self.plasma_line_counter -= 1

    def remove_last_nbi_line(self):
        if self.nbi_line_counter > 0:
            self.remove_last_line(self.nbi_line_counter, self.nbi_lines_layout)
            self.nbi_line_counter -= 1

    def remove_last_dnb_line(self):
        if self.dnb_line_counter > 0:
            self.remove_last_line(self.dnb_line_counter, self.dnb_lines_layout)
            self.dnb_line_counter -= 1

    def remove_last_line(self, counter, layout):
        columns = layout.columnCount()
        for column in range(columns):
            item = layout.itemAtPosition(counter - 1, column)
            if item is not None:
                item.widget().deleteLater()
                layout.removeItem(item)


    def retrieve_lines_from_layout(self, layout, element = None, verbose : bool = False):
        rows = layout.rowCount()
        columns = layout.columnCount()
        line_list =  []

        if columns == 8:
            for row in range(rows):
                item = layout.itemAtPosition(row, 0)
                if (item is not None) and (not isinstance(item, QSpacerItem)):
                    element_widget    = layout.itemAtPosition(row, 1).widget()
                    charge_widget     = layout.itemAtPosition(row, 2).widget()
                    high_level_widget = layout.itemAtPosition(row, 3).widget()
                    low_level_widget  = layout.itemAtPosition(row, 5).widget()
                    type_widget       = layout.itemAtPosition(row, 6).widget()
                    shape_widget      = layout.itemAtPosition(row, 7).widget()

                    new_line = line_settings(element_widget.currentText(), charge_widget.value(), high_level_widget.value(), low_level_widget.value(), type_widget.currentText(), shape_widget.currentText())
                    line_list.append(new_line)
                    if verbose:
                        line_list[-1].verbose()

        elif columns == 6:
            for row in range(rows):
                item = layout.itemAtPosition(row, 0)
                if (item is not None) and (not isinstance(item, QSpacerItem)):
                    type_widget       = layout.itemAtPosition(row, 1).widget()
                    high_level_widget = layout.itemAtPosition(row, 2).widget()
                    low_level_widget  = layout.itemAtPosition(row, 4).widget()
                    shape_widget      = layout.itemAtPosition(row, 5).widget()

                    new_line = line_settings(element, 0, high_level_widget.value(), low_level_widget.value(), type_widget.currentText(), shape_widget.text())
                    line_list.append(new_line)

                    if verbose:
                        line_list[-1].verbose()

        return line_list


    def observer_selection_toggled(self):
        self.ui.observer_specific_parameters.removeItem(self.ui.observer_extra_spacer)
        for i in reversed(range(self.ui.observer_specific_parameters.count())):
            self.ui.observer_specific_parameters.itemAt(i).widget().deleteLater()

        selected = self.ui.comboBox_observer_select.currentText()
        if selected == 'Optical Fiber':
            acceptance    = inline_widget('Acceptance angle, deg', True, 45, [0,90])
            radius        = inline_widget('Fiber Radius, m', True, 0.0005, [0,1], 5)
            spectral_rays = inline_widget('Spectral statistic level', False, 4, [0,6])

            self.ui.observer_specific_parameters.addWidget(acceptance)
            self.ui.observer_specific_parameters.addWidget(radius)
            self.ui.observer_specific_parameters.addWidget(spectral_rays)

        elif selected == 'Pinhole Camera':
            camera_x = inline_widget('Camera horisontal resolution, pix', False, 128, [4,8192])
            camera_y = inline_widget('Camera vertical resolution, pix', False, 128, [4,8192])
            camera_fov = inline_widget('Camera field of view, deg', True, 75, [1,180], 1)
            samples_level = inline_widget('Pixel statistic level', False, 4, [0,6])

            self.ui.observer_specific_parameters.addWidget(camera_x)
            self.ui.observer_specific_parameters.addWidget(camera_y)
            self.ui.observer_specific_parameters.addWidget(camera_fov)
            self.ui.observer_specific_parameters.addWidget(samples_level)

        self.ui.observer_specific_parameters.addItem(self.ui.observer_extra_spacer)

    def retrieve_observer_settings(self):
        self.settings.observer = None

        start            = [self.ui.observer_start_x.value(), self.ui.observer_start_y.value(), self.ui.observer_start_z.value()]
        target           = [self.ui.observer_target_x.value(), self.ui.observer_target_y.value(), self.ui.observer_target_z.value()]
        wavelength_range = [self.ui.min_wl_spin.value(), self.ui.max_wl_spin.value()]
        samples_per_task = self.ui.samples_per_task_spin.value()
        bins_level       = self.ui.sampling_level.value()
        reflections      = self.ui.check_reflections.isChecked()

        if reflections:
            ref_level = self.ui.pixel_samples_level.value()
        else:
            ref_level = 1

        selected = self.ui.comboBox_observer_select.currentText()
        if selected == 'Optical Fiber':
            acceptance    = self.ui.observer_specific_parameters.itemAt(0).widget().layout().itemAt(2).widget().value()
            radius        = self.ui.observer_specific_parameters.itemAt(1).widget().layout().itemAt(2).widget().value()
            spectral_rays = self.ui.observer_specific_parameters.itemAt(2).widget().layout().itemAt(2).widget().value()

            observer      = fibre_optic(start, target, wavelength_range, samples_per_task, bins_level, acceptance, radius, spectral_rays, reflections, ref_level)
        elif selected == 'Pinhole Camera':
            resolution_x  = self.ui.observer_specific_parameters.itemAt(0).widget().layout().itemAt(2).widget().value()
            resolution_y  = self.ui.observer_specific_parameters.itemAt(1).widget().layout().itemAt(2).widget().value()
            fov           = self.ui.observer_specific_parameters.itemAt(2).widget().layout().itemAt(2).widget().value()
            quality_level = self.ui.observer_specific_parameters.itemAt(3).widget().layout().itemAt(2).widget().value()
            pixels        = [resolution_x, resolution_y]

            observer = pinhole_camera(start, target, wavelength_range, samples_per_task, bins_level, fov, pixels, quality_level, reflections, ref_level)
        else:
            observer = line_of_sight(start, target, wavelength_range, samples_per_task, bins_level, reflections, ref_level)

        return observer




    def reflection_check_toggled(self):
        if self.ui.check_reflections.isChecked():
            self.ui.pixel_samples_level.setEnabled(True)
        else:
            self.ui.pixel_samples_level.setEnabled(False)

    @system_check
    def check_lines_pushed(self):
        plasma_lines = self.retrieve_lines_from_layout(self.plasma_lines_layout)
        dnb_lines    = self.retrieve_lines_from_layout(self.dnb_lines_layout, self.dnb_element)
        nbi_lines    = self.retrieve_lines_from_layout(self.nbi_lines_layout, self.nbi_element)

        create_plasma_lines(plasma_lines, True)
        create_beam_lines(dnb_lines, self.plasma_species_list[0], True)
        create_beam_lines(nbi_lines, self.plasma_species_list[0], True)




    @system_check
    def check_observer_pushed(self):
        pass


    def start_sumulation(self):
        get_element('helium')

        self.settings.plasma_models = self.retrieve_lines_from_layout(self.plasma_lines_layout)
        self.settings.dnb_models    = self.retrieve_lines_from_layout(self.dnb_lines_layout, self.dnb_element)
        self.settings.nbi_models    = self.retrieve_lines_from_layout(self.nbi_lines_layout, self.nbi_element)
        self.settings.observer      = self.retrieve_observer_settings()

        self.settings.verbose()

